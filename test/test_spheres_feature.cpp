#include <iostream>
#include "gtest/gtest.h"
#include "sphere.h"

using namespace ray_tracer;

TEST(spheres_feature, intersect_ray_and_sphere) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  /* Sphere s; */
  const auto s = Sphere::create();

  /* std::vector<double> result = s.intersect(r); */
  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 2);
  ASSERT_TRUE(result[0].get_t() == 4.);
  ASSERT_TRUE(result[1].get_t() == 6.);
}

TEST(spheres_feature, ray_misses_sphere) {
  Ray r(point(0., 2., -5.), vector(0., 0., 1.));
  const auto s = Sphere::create();

  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 0);
}

TEST(spheres_feature, ray_originates_inside_sphere) {
  Ray r(point(0., 0., 0.), vector(0., 0., 1.));
  const auto s = Sphere::create();

  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 2);
  ASSERT_TRUE(result[0].get_t() == -1.);
  ASSERT_TRUE(result[1].get_t() == 1.);
}

TEST(spheres_feature, sphere_behind_ray) {
  Ray r(point(0., 0., 5.), vector(0., 0., 1.));
  const auto s = Sphere::create();

  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 2);
  ASSERT_TRUE(result[0].get_t() == -6.);
  ASSERT_TRUE(result[1].get_t() == -4.);
}

TEST(spheres_feature, sphere_default_transformation) {
  const auto s = Sphere::create();

  ASSERT_TRUE(s->get_transform() == identity(4));
}

TEST(spheres_feature, sphere_set_transformation) {
  const auto s = Sphere::create();
  Matrix t = translation(2., 3., 4.);

  s->set_transform(t);
  ASSERT_TRUE(s->get_transform() == t);
}

TEST(spheres_feature, intersecting_scaled_sphere_with_ray) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  const auto s = Sphere::create();

  s->set_transform(scaling(2., 2., 2.));

  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 2);
  ASSERT_TRUE(result[0].get_t() == 3.);
  ASSERT_TRUE(result[1].get_t() == 7.);
}

TEST(spheres_feature, intersecting_translated_sphere_with_ray) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  const auto s = Sphere::create();

  s->set_transform(translation(5., 0., 0.));

  std::vector<Intersection> result = s->intersect(r);

  ASSERT_TRUE(result.size() == 0);
}

TEST(spheres_feature, normal_x_axis) {
  const auto s = Sphere::create();

  Tuple normal = s->normal_at(point(1., 0., 0.));

  ASSERT_TRUE(normal == vector(1., 0., 0.));
}

TEST(spheres_feature, normal_y_axis) {
  const auto s = Sphere::create();

  Tuple normal = s->normal_at(point(0., 1., 0.));

  ASSERT_TRUE(normal == vector(0., 1., 0.));
}

TEST(spheres_feature, normal_z_axis) {
  const auto s = Sphere::create();

  Tuple normal = s->normal_at(point(0., 0., 1.));

  ASSERT_TRUE(normal == vector(0., 0., 1.));
}

TEST(spheres_feature, normal_nonaxial) {
  const auto s = Sphere::create();

  Tuple normal = s->normal_at(point(std::sqrt(3.)/3., std::sqrt(3.)/3., std::sqrt(3.)/3.));

  ASSERT_TRUE(normal == vector(std::sqrt(3.)/3., std::sqrt(3.)/3., std::sqrt(3.)/3.));
}

TEST(spheres_feature, normal_is_normalized) {
  const auto s = Sphere::create();

  Tuple normal = s->normal_at(point(std::sqrt(3.)/3., std::sqrt(3.)/3., std::sqrt(3.)/3.));

  ASSERT_TRUE(normal == normal.normalize());
}

TEST(spheres_feature, normal_on_translated_sphere) {
  const auto s = Sphere::create();

  s->set_transform(translation(0., 1., 0.));

  Tuple normal = s->normal_at(point(0., 1.70711, -0.70711));

  ASSERT_TRUE(normal == vector(0., 0.70711, -0.70711));
}

TEST(spheres_feature, normal_on_transformed_sphere) {
  const auto s = Sphere::create();

  s->set_transform(scaling(1., 0.5, 1.) * rotation_z(pi()/5));

  Tuple normal = s->normal_at(point(0., std::sqrt(2)/2, -std::sqrt(2)/2));

  ASSERT_TRUE(normal == vector(0., 0.97014, -0.24254));
}

TEST(spheres_feature, sphere_has_default_material) {
  const auto s = Sphere::create();

  ASSERT_TRUE(s->get_material() == Material());
}

TEST(spheres_feature, sphere_assign_material) {
  const auto s = Sphere::create();

  Material m;

  m.set_ambient(1);
  
  s->set_material(m);

  ASSERT_TRUE(s->get_material() == m);
}

TEST(spheres_feature, sphere_compare) {
  const auto s1 = Sphere::create();
  const auto s2 = Sphere::create();


  ASSERT_TRUE(*s1 == *s2);
}

TEST(spheres_feature, helper_for_producing_sphere_with_glassy_material) {
  const auto s = Sphere::create_glass_sphere();

  ASSERT_TRUE(s->get_transform() == identity(4));
  ASSERT_TRUE(s->get_material().get_transparency() == 1.);
  ASSERT_TRUE(s->get_material().get_refractive_index() == 1.5);


}



