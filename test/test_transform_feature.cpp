#include <iostream>
#include "gtest/gtest.h"
#include "transform.h"

using namespace ray_tracer;

TEST(transform_features, translation)
{
  Tuple p = point(-3., 4., 5.);
  Matrix transform = translation(5., -3., 2.);

  Tuple result = transform * p;

  /* std::cout << p << std::endl; */
  /* std::cout << transform << std::endl; */
  /* std::cout << result << std::endl; */

  ASSERT_TRUE(result == point(2., 1., 7.));
}

TEST(transform_features, inverse_translation)
{
  Tuple p = point(-3., 4., 5.);
  Matrix inv_transform = inverse(translation(5., -3., 2.));

  Tuple result = inv_transform * p;

  ASSERT_TRUE(result == point(-8., 7., 3.));
}

TEST(transform_features, translation_vector)
{
  Tuple v = vector(-3., 4., 5.);
  Matrix transform = translation(5., -3., 2.);

  Tuple result = transform * v;

  ASSERT_TRUE(result == v);
}

TEST(transform_features, scaling)
{
  Tuple v = vector(-4., 6., 8.);
  Matrix transform = scaling(2., 3., 4.);

  Tuple result = transform * v;

  ASSERT_TRUE(result == vector(-8., 18., 32.));
}

TEST(transform_features, inverse_scaling)
{
  Tuple v = vector(-4., 6., 8.);
  Matrix transform = inverse(scaling(2., 3., 4.));

  Tuple result = transform * v;

  ASSERT_TRUE(result == vector(-2., 2., 2.));
}

TEST(transform_features, reflection)
{
  Tuple p = point(2., 3., 4.);
  Matrix transform = scaling(-1., 1., 1.);

  Tuple result = transform * p;

  ASSERT_TRUE(result == point(-2., 3., 4.));
}

TEST(transform_features, rotation_x)
{
  Tuple p = point(0., 1., 0.);
  Matrix half_quarter = rotation_x(pi()/4);
  Matrix full_quarter = rotation_x(pi()/2);

  ASSERT_TRUE(half_quarter * p == point(0., std::sqrt(2.)/2., std::sqrt(2.)/2.));
  ASSERT_TRUE(full_quarter * p == point(0., 0., 1.));
}

TEST(transform_features, inverse_rotation_x)
{
  Tuple p = point(0., 1., 0.);
  Matrix half_quarter = rotation_x(pi()/4);
  Matrix inv = inverse(half_quarter);

  ASSERT_TRUE(inv * p == point(0., std::sqrt(2.)/2., -std::sqrt(2.)/2.));
}

TEST(transform_features, rotation_y)
{
  Tuple p = point(0., 0., 1.);
  Matrix half_quarter = rotation_y(pi()/4);
  Matrix full_quarter = rotation_y(pi()/2);

  ASSERT_TRUE(half_quarter * p == point(std::sqrt(2.)/2., 0., std::sqrt(2.)/2.));
  ASSERT_TRUE(full_quarter * p == point(1., 0., 0.));
}

TEST(transform_features, rotation_z)
{
  Tuple p = point(0., 1., 0.);
  Matrix half_quarter = rotation_z(pi()/4);
  Matrix full_quarter = rotation_z(pi()/2);

  ASSERT_TRUE(half_quarter * p == point(-std::sqrt(2.)/2., std::sqrt(2.)/2., 0.));
  ASSERT_TRUE(full_quarter * p == point(-1., 0., 0.));
}

TEST(transform_features, shearing_move_x_inPropTo_y)
{
  Matrix transform = shearing(1., 0., 0., 0., 0., 0.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(5., 3., 4.));
}

TEST(transform_features, shearing_move_x_inPropTo_z)
{
  Matrix transform = shearing(0., 1., 0., 0., 0., 0.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(6., 3., 4.));
}

TEST(transform_features, shearing_move_y_inPropTo_x)
{
  Matrix transform = shearing(0., 0., 1., 0., 0., 0.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(2., 5., 4.));
}

TEST(transform_features, shearing_move_y_inPropTo_z)
{
  Matrix transform = shearing(0., 0., 0., 1., 0., 0.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(2., 7., 4.));
}

TEST(transform_features, shearing_move_z_inPropTo_x)
{
  Matrix transform = shearing(0., 0., 0., 0., 1., 0.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(2., 3., 6.));
}

TEST(transform_features, shearing_move_z_inPropTo_y)
{
  Matrix transform = shearing(0., 0., 0., 0., 0., 1.);
  Tuple p = point(2., 3., 4.);

  ASSERT_TRUE(transform * p == point(2., 3., 7.));
}

TEST(transform_features, transforms_in_sequence)
{
  Tuple p = point(1., 0., 1.);
  Matrix A = rotation_x(pi()/2);
  Matrix B = scaling(5., 5., 5.);
  Matrix C = translation(10., 5., 7.);

  Tuple p2 = A * p;
  Tuple p3 = B * p2;
  Tuple p4 = C * p3;

  ASSERT_TRUE(p2 == point(1., -1., 0.));
  ASSERT_TRUE(p3 == point(5., -5., 0.));
  ASSERT_TRUE(p4 == point(15., 0., 7.));
}

TEST(transform_features, chained_transforms_reverse)
{
  Tuple p = point(1., 0., 1.);
  Matrix A = rotation_x(pi()/2);
  Matrix B = scaling(5., 5., 5.);
  Matrix C = translation(10., 5., 7.);

  Matrix T = C * B * A;

  ASSERT_TRUE(T * p == point(15., 0., 7.));
}

TEST(transform_features, view_transform_for_default_orientation)
{
  Tuple from = point(0., 0., 0.);
  Tuple to = point(0., 0., -1.);
  Tuple up = vector(0., 1., 0.);

  Matrix result = view_transform(from, to, up);

  ASSERT_TRUE(result == identity(4));
}

TEST(transform_features, view_transform_for_looking_in_pos_z_direction)
{
  Tuple from = point(0., 0., 0.);
  Tuple to = point(0., 0., 1.);
  Tuple up = vector(0., 1., 0.);

  Matrix result = view_transform(from, to, up);

  ASSERT_TRUE(result == scaling(-1., 1., -1.));
}

TEST(transform_features, view_transform_moves_world)
{
  Tuple from = point(0., 0., 8.);
  Tuple to = point(0., 0., 0.);
  Tuple up = vector(0., 1., 0.);

  Matrix result = view_transform(from, to, up);

  ASSERT_TRUE(result == translation(0., 0., -8.));
}

TEST(transform_features, arbitrary_view_transform)
{
  Tuple from = point(1., 3., 2.);
  Tuple to = point(4., -2., 8.);
  Tuple up = vector(1., 1., 0.);

  Matrix result = view_transform(from, to, up);

  Matrix expected = identity(4);
  expected[0][0] = -0.50709;
  expected[0][1] = 0.50709;
  expected[0][2] = 0.67612;
  expected[0][3] = -2.36643;
   
  expected[1][0] = 0.76772;
  expected[1][1] = 0.60609;
  expected[1][2] = 0.12122;
  expected[1][3] = -2.82843;

  expected[2][0] = -0.35857;
  expected[2][1] = 0.59761;
  expected[2][2] = -0.71714;
  expected[2][3] = 0.;

  expected[3][0] = 0.;
  expected[3][1] = 0.;
  expected[3][2] = 0.;
  expected[3][3] = 1.;


  ASSERT_TRUE(result == expected);
}

