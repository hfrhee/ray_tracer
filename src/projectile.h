#pragma once

#include "tuple.h"

namespace ray_tracer {

  class Projectile {
    public:
      Tuple position;
      Tuple velocity;

      Projectile() {
        position = point(0., 0., 0.);
        velocity = vector(0., 0., 0.);
      }

      Projectile(const Tuple& position, const Tuple& velocity)
        : position(position), velocity(velocity) {
        assert(this->position.is_point());
        assert(this->velocity.is_vector());
      }
  };

}
