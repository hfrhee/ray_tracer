#pragma once

#include "tuple.h"

namespace ray_tracer {

  class Environment {
    public:
      Tuple gravity;
      Tuple wind;

      Environment() {
        gravity = vector(0., 0., 0.);    
        wind = vector(0., 0., 0.);    
      }

      Environment(Tuple gravity, Tuple wind) {
        assert(gravity.is_vector());
        assert(wind.is_vector());

        this->gravity = gravity;
        this->wind = wind;
      }
  };

}
