#include "sphere.h"

namespace ray_tracer{

  std::shared_ptr<Sphere> Sphere::create() {
    return std::make_shared<Sphere>( Sphere() );
  }

  std::shared_ptr<Sphere> Sphere::create_glass_sphere() {
    auto result = std::make_shared<Sphere>(Sphere()); 

    result->get_material().set_transparency(1.);
    result->get_material().set_refractive_index(1.5);

    return result;
  }

  bool Sphere::operator==(const Sphere& other) const {
    bool result;

    result = get_name() == other.get_name();
    result = result && (get_transform() == other.get_transform());
    result = result && (get_material() == other.get_material());

    return result;
  }

  bool Sphere::doCompare(const Object& other) const {
    const auto *s = dynamic_cast<const Sphere*>(&other); 

    return (s != nullptr && *this == *s);
  } 

  /* std::vector<Intersection> Sphere::local_intersect(const Ray& r_in) const { */
  std::vector<Intersection> Sphere::local_intersect(const Ray& r) const {
    std::vector<Intersection> result; 

    Tuple sphere_to_ray = r.origin - this->center;
    double a = dot(r.direction, r.direction); 
    double b = 2 * dot(r.direction, sphere_to_ray);
    double c = dot(sphere_to_ray, sphere_to_ray) - this->radius;

    double discriminant = b*b - 4*a*c; 

    if(discriminant < 0) return result;

    double t0 = (-b - std::sqrt(discriminant)) / (2*a);
    double t1 = (-b + std::sqrt(discriminant)) / (2*a);

    result.emplace_back(Intersection(t0, shared_from_this()));
    result.emplace_back(Intersection(t1, shared_from_this()));
  
    return result;
  }
  
  Tuple Sphere::local_normal_at(const Tuple& point_in) const {
    assert(point_in.is_point());
    return (point_in - center).normalize();
  } 
}
