#include <iostream>
#include <fstream>
#include "common.h"
#include "sphere.h"
#include "material.h"
#include "world.h"
#include "camera.h"
#include "canvas.h"


using namespace ray_tracer;

int main() {
  const auto floor = Sphere::create();
  floor->set_transform(scaling(10, 0.01, 10.));
  floor->set_material(Material());
  floor->get_material().set_color(Color(1., 0.9, 0.9));
  floor->get_material().set_specular(0.);

  const auto left_wall = Sphere::create();
  left_wall->set_transform(translation(0., 0., 5.) *
      rotation_y(-pi()/4) *
      rotation_x(pi()/2) *
      scaling(10., 0.01, 10.));
  left_wall->set_material(floor->get_material());

  
  const auto right_wall = Sphere::create();
  right_wall->set_transform(translation(0., 0., 5.) *
      rotation_y(pi()/4) *
      rotation_x(pi()/2) *
      scaling(10., 0.01, 10.));
  right_wall->set_material(floor->get_material());

  const auto middle = Sphere::create();
  middle->set_transform(translation(-.5, 1., .5));
  middle->set_material(Material());
  middle->get_material().set_color(Color(.1, 1., .5));
  middle->get_material().set_diffuse(.7);
  middle->get_material().set_specular(.3);
  
  const auto right = Sphere::create();
  right->set_transform(translation(1.5, .5, -.5) *
      scaling(.5, .5, .5));
  right->set_material(Material());
  right->get_material().set_color(Color(.5, 1., .1));
  right->get_material().set_diffuse(.7);
  right->get_material().set_specular(.3);
  
  const auto left = Sphere::create();
  left->set_transform(translation(-1.5, .33, -.75) *
      scaling(.33, .33, .33));
  left->set_material(Material());
  left->get_material().set_color(Color(1., .8, .1));
  left->get_material().set_diffuse(.7);
  left->get_material().set_specular(.3);

  Light point_light(point(-10., 10., -10.), Color(1., 1., 1.));

  std::vector<Light> lights {point_light};
  std::vector<std::shared_ptr<Object> > objects {floor, right_wall, left_wall, middle, right, left};

  World world(objects, lights);
  Camera camera(300, 250, pi()/3);
  /* Camera camera(100, 50, pi()/3); */

  camera.set_transform(view_transform(point(0., 1.5, -5.), 
        point(0., 1., 0.),
        vector(0., 1., 0.)));

  Canvas canvas = camera.render(world);

  std::ofstream img("img07.ppm");
  canvas.to_ppm(img);
  img.close();

  return 0;
}
