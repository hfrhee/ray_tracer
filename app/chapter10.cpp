#include <iostream>
#include <fstream>
#include "common.h"
#include "plane.h"
#include "sphere.h"
#include "material.h"
#include "world.h"
#include "camera.h"
#include "canvas.h"
#include "stripe_pattern.h"
#include "gradient_pattern.h"
#include "ring_pattern.h"
#include "checkers_pattern.h"


using namespace ray_tracer;

int main() {
  const auto floor = Plane::create();
  floor->set_material(Material());
  floor->get_material().set_color(Color(1., 0.9, 0.9));
  floor->get_material().set_specular(0.);

  const auto floor_pattern = std::make_shared<CheckersPattern>
    (CheckersPattern(color::white, color::black));
  floor->get_material().set_pattern(floor_pattern);
  
  const auto left_wall = Plane::create();
  left_wall->set_material(Material());
  left_wall->get_material().set_color(Color(1., 0.9, 0.9));
  left_wall->get_material().set_specular(0.);
  left_wall->set_transform(translation(0., 0., 5.) *
      rotation_y(-pi()/4) *
      rotation_x(pi()/2));

  const auto left_wall_pattern = std::make_shared<RingPattern>
    (RingPattern(color::white, color::black));
  left_wall_pattern->set_transform(translation(-3., 0., -1.5) * scaling(0.5, 0.5, 0.5));
  left_wall->get_material().set_pattern(left_wall_pattern);

  const auto right_wall = Plane::create();
  right_wall->set_material(Material());
  right_wall->get_material().set_color(Color(1., 0.9, 0.9));
  right_wall->get_material().set_specular(0.);
  right_wall->set_transform(translation(0., 0., 7.) *
      rotation_y(pi()/4) *
      rotation_x(pi()/2));
  const auto right_wall_pattern = std::make_shared<GradientPattern>
    (GradientPattern(color::white, color::black));
  right_wall_pattern->set_transform(scaling(7., 1., 1.));
  right_wall->get_material().set_pattern(right_wall_pattern);

  const auto middle = Sphere::create();
  /* middle->set_transform(translation(-.5, 1., .5)); */
  middle->set_transform(translation(1.5, 1., .5));
  middle->set_material(Material());
  middle->get_material().set_color(Color(.1, 1., .5));
  middle->get_material().set_diffuse(.7);
  middle->get_material().set_specular(.3);
  
  const auto middle_pattern = std::make_shared<StripePattern>
    (StripePattern(color::white, Color(1., 0., 0.)));
  middle_pattern->set_transform(scaling(.25, .25, .25));
  /* const auto middle_pattern = std::make_shared<StripePattern> */
  /*   (StripePattern(color::white, Color(1., 0., 0.))); */
  /* const auto middle_pattern = std::make_shared<CheckersPattern> */
  /*   (CheckersPattern(color::white, Color(1., 0., 0.))); */
  /* middle_pattern->set_transform(scaling(.5, .5, .5)); */

  middle->get_material().set_pattern(middle_pattern);

  /* const auto right = Sphere::create(); */
  /* right->set_transform(translation(1.5, .5, -.5) * */
  /*     scaling(.5, .5, .5)); */
  /* right->set_material(Material()); */
  /* right->get_material().set_color(Color(.5, 1., .1)); */
  /* right->get_material().set_diffuse(.7); */
  /* right->get_material().set_specular(.3); */
  
  /* const auto left = Sphere::create(); */
  /* left->set_transform(translation(-1.5, .33, -.75) * */
  /*     scaling(.33, .33, .33)); */
  /* left->set_material(Material()); */
  /* left->get_material().set_color(Color(1., .8, .1)); */
  /* left->get_material().set_diffuse(.7); */
  /* left->get_material().set_specular(.3); */

  Light point_light(point(-10., 10., -10.), Color(1., 1., 1.));

  std::vector<Light> lights {point_light};
  std::vector<std::shared_ptr<Object> > objects {floor,
    left_wall,
    right_wall,
    middle /*, right, left*/};

  World world(objects, lights);
  Camera camera(300, 250, pi()/3);
  /* Camera camera(100, 50, pi()/3); */

  camera.set_transform(view_transform(point(0., 1.5, -5.), 
        point(0., 1., 0.),
        vector(0., 1., 0.)));

  Canvas canvas = camera.render(world);

  std::ofstream img("img10.ppm");
  canvas.to_ppm(img);
  img.close();

  return 0;
}
