#include <iostream>
#include "gtest/gtest.h"
#include "test_shapes_feature.h"
#include "object.h"

using namespace ray_tracer;

TEST(sphapes_feature, default_transformation) {
  Test_Shape s;

  ASSERT_TRUE(s.get_transform() == identity(4));
}

TEST(sphapes_feature, assign_transformation) {
  Test_Shape s;

  s.set_transform(translation(2., 3., 4.));

  ASSERT_TRUE(s.get_transform() == translation(2., 3., 4.));
}

TEST(sphapes_feature, default_material) {
  Test_Shape s;

  ASSERT_TRUE(s.get_material() == Material());
}

TEST(sphapes_feature, assign_material) {
  Test_Shape s;

  Material m;
  m.set_ambient(1.);

  s.set_material(m);

  ASSERT_TRUE(s.get_material() == m);
}

TEST(sphapes_feature, intersecting_scaled_shape_with_ray) {
  Ray r(point(0., 0., -5), vector(0., 0., 1.));

  Test_Shape s;
  s.set_transform(scaling(2., 2., 2.));

  const auto xs = s.intersect(r);

  ASSERT_TRUE(s.saved_ray.origin == point(0., 0., -2.5));
  ASSERT_TRUE(s.saved_ray.direction== vector(0., 0., .5));
}

TEST(sphapes_feature, intersecting_translated_shape_with_ray) {
  Ray r(point(0., 0., -5), vector(0., 0., 1.));

  Test_Shape s;
  s.set_transform(translation(5., 0., 0.));

  const auto xs = s.intersect(r);

  ASSERT_TRUE(s.saved_ray.origin == point(-5., 0., -5.));
  ASSERT_TRUE(s.saved_ray.direction== vector(0., 0., 1.));
}

TEST(sphapes_feature, computing_normal_on_translated_shape) {
  Test_Shape s;
  s.set_transform(translation(0., 1., 0.));

  Tuple n = s.normal_at(point(0., 1.70711, -0.70711));

  ASSERT_TRUE(n == vector(0., .70711, -0.70711));
}

TEST(sphapes_feature, computing_normal_on_transformed_shape) {
  Test_Shape s;
  s.set_transform(scaling(1., .5, 1.) * rotation_z(pi()/5));

  Tuple n = s.normal_at(point(0., std::sqrt(2)/2, -std::sqrt(2)/2));

  ASSERT_TRUE(n == vector(0., .97014, -0.24254));
}
