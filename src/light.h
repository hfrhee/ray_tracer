#pragma once
#include "tuple.h"
#include "color.h"

namespace ray_tracer {
  class Light {
    public:
      Light(const Tuple& position, const Color& intensity)
        : position(position), intensity(intensity) {}

      bool operator==(const Light& other) const {
        return (position == other.get_position()) && (intensity == other.get_intensity());
      } 

      Tuple get_position() const { return position; }
      Color get_intensity() const { return intensity; }

    private:
      Tuple position;
      Color intensity;


  };
}
