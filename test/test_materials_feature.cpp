#include "gtest/gtest.h"
#include "material.h"
#include "light.h"
#include "stripe_pattern.h"
#include "sphere.h"

using namespace ray_tracer;

TEST(materials_feature, default_material)
{
  Material m;
  ASSERT_TRUE(m.get_color() == Color(1., 1., 1.));
  ASSERT_TRUE(m.get_ambient() == 0.1);
  ASSERT_TRUE(m.get_diffuse() == 0.9);
  ASSERT_TRUE(m.get_specular() == 0.9);
  ASSERT_TRUE(m.get_shininess() == 200.);
}

TEST(materials_feature, material_set_get)
{
  Material m;

  m.set_color(Color(.5, .5, .5));
  m.set_ambient(.5);
  m.set_diffuse(.5);
  m.set_specular(.5);
  m.set_shininess(50);
  ASSERT_TRUE(m.get_color() == Color(.5, .5, .5));
  ASSERT_TRUE(m.get_ambient() == .5);
  ASSERT_TRUE(m.get_diffuse() == .5);
  ASSERT_TRUE(m.get_specular() == .5);
  ASSERT_TRUE(m.get_shininess() == 50);
}

TEST(materials_feature, light_eye_surface)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., 0., -1.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 0., -10.), Color(1., 1., 1.));

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, false);

  ASSERT_TRUE(result == Color(1.9, 1.9, 1.9));
}

TEST(materials_feature, light_eye_45degrees_surface)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., std::sqrt(2.)/2.,std::sqrt(2.)/2.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 0., -10.), Color(1., 1., 1.));

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, false);

  ASSERT_TRUE(result == Color(1., 1., 1.));
}

TEST(materials_feature, light45_degrees_eye_surface)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., 0., -1.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 10., -10.), Color(1., 1., 1.));

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, false);

  ASSERT_TRUE(result == Color(.7364, .7364, .7364));
}

TEST(materials_feature, light_with_eye_in_path_of_reflection)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., -std::sqrt(2.)/2., -std::sqrt(2.)/2.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 10., -10.), Color(1., 1., 1.));

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, false);

  ASSERT_TRUE(result == Color(1.6364, 1.6364, 1.6364));
}

TEST(materials_feature, light_behind_surface)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., 0., -1.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 0., 10.), Color(1., 1., 1.));

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, false);

  ASSERT_TRUE(result == Color(.1, .1, .1));
}

TEST(materials_feature, lighting_with_surface_in_shadow)
{
  Material m;
  Tuple position = point(0., 0., 0.);

  Tuple eyev = vector(0., 0., -1.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 0., -10.), Color(1., 1., 1.));

  bool in_shadow = true;

  Color result = m.lighting(*Sphere::create(), light, position, eyev, normalv, in_shadow);

  ASSERT_TRUE(result == Color(.1, .1, .1));

}

TEST(materials_feature, lighting_with_pattern)
{
  const auto pattern = std::make_shared<StripePattern>(StripePattern(color::white, color::black));
  Material m;
  m.set_ambient(1.);
  m.set_diffuse(0.);
  m.set_specular(0.);
  m.set_pattern(pattern);

  Tuple eyev = vector(0., 0., -1.);
  Tuple normalv = vector(0., 0., -1.);
  Light light(point(0., 0., -10.), Color(1., 1., 1.));

  Color c1 = m.lighting(*Sphere::create(), light, point(.9, 0., 0.), eyev, normalv, false);
  Color c2 = m.lighting(*Sphere::create(), light, point(1.1, 0., 0.), eyev, normalv, false);

  ASSERT_TRUE(c1 == Color(1., 1., 1.));
  ASSERT_TRUE(c2 == Color(0., 0., 0.));

}

TEST(materials_feature, reflectivity_for_default_material)
{
  Material m;

  ASSERT_TRUE(m.get_reflective() == 0.);
}

TEST(materials_feature, transparency_and_refractive_index_for_default_material)
{
  Material m;

  ASSERT_TRUE(m.get_transparency() == 0.);
  ASSERT_TRUE(m.get_refractive_index() == 1.);
}



