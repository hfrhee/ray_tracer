#include "canvas.h"

namespace ray_tracer {

  Canvas::Canvas(int width ,int height) : width(width), height(height) {
    pixel = std::vector< std::vector<Color> >(width, std::vector<Color>(height));

    for (int y=0; y < height; ++y) {
      for (int x=0; x < width; ++x) {
        pixel[x][y] = Color(0., 0., 0.);
      }
    }
  }

  Color Canvas::pixel_at(int x, int y) const {
    assert(y < height);
    assert(x < width);
    return pixel[x][y]; 
  }

  void Canvas::write_pixel(int x, int y, Color color) {
    assert(y < height && x < width);
    pixel[x][y] = color;
  }

  int count_digits(int val) {
    int result = 0;

    while (val != 0) {
      val = val / 10;
      ++result; 
    }
  
    return result;
  }

  void Canvas::to_ppm(std::ostream &os) const {
    int max_char_per_line = 70;

    os << "P3\n";
    os << width << " " << height << std::endl;
    os << Color::max_value << std::endl;

    int char_count = 0;

    for (int y=0; y<height; ++y) {
      for (int x=0; x<width; ++x) {

        for (int rgb=0; rgb<3; ++rgb) {
          int rgb_val = (int)(Color::max_value * (this->pixel_at(x,y)[rgb]) + .5);
          rgb_val = std::max(0, std::min(rgb_val, 255));
          
          char_count += count_digits(rgb_val);
          
          if (char_count < max_char_per_line) {
            os << " ";
            ++char_count;
          } else if (char_count >= max_char_per_line) {
            os << '\n';
            char_count = 0;
          }

          os << rgb_val;
        }

      }
      os << '\n';
      char_count = 0;
    }

  }
}
