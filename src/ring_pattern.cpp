#include "common.h"
#include "ring_pattern.h"

namespace ray_tracer {

  Color RingPattern::pattern_at(const Tuple& point) const {
    assert(point.is_point());

    double tmp = std::sqrt(point[0]*point[0] + point[2]*point[2]);

    return (static_cast<int>(std::floor(tmp)) % 2) ? b : a;
  }
  
} 
