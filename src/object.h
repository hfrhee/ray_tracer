#pragma once

#include <string>
#include <vector>

#include "ray.h"
/* #include "matrix.h" */
#include "intersection.h"
#include "transform.h"
#include "material.h"

namespace ray_tracer {

  class Intersection;
  /* class Material; */

  class Object : public std::enable_shared_from_this<Object> {
    public:

      Object(const std::string& s) : name(s), transform(identity(4)) {}

      virtual ~Object() = default;

      virtual bool operator==(const Object& other) const {
        return doCompare(other);
      } 

      std::string get_name() const { return name; }

      const Matrix& get_transform() const { return transform; }
      void set_transform(const Matrix& t) { transform = t; }

      const Material& get_material() const { return material; }
      Material& get_material() { return material; }
      void set_material(const Material& other) { material = other; }

      std::vector<Intersection> intersect(const Ray& r) const;
      Tuple normal_at(const Tuple& point) const;


    protected:
      virtual bool doCompare(const Object& other) const = 0;
      virtual std::vector<Intersection> local_intersect(const Ray& r) const = 0;
      virtual Tuple local_normal_at(const Tuple& point) const = 0;

    private:
      std::string name;
      Matrix transform;
      Material material;

  };
}
