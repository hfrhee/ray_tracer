#include "common.h"
#include "pattern.h"

namespace ray_tracer {

  Color Pattern::pattern_at_shape(const Object& object, const Tuple& point) const {
    Color result;

    Tuple object_point = inverse(object.get_transform()) * point; 
    Tuple pattern_point = inverse(transform) * object_point;

    result = pattern_at(pattern_point);

    return result;
  }

} 
