#include <iostream>

#include "projectile.h"
#include "environment.h"

using namespace ray_tracer;

Projectile tick(const Environment& env, const Projectile& proj) {
  Projectile result;
  
  result.position = proj.position + proj.velocity;
  result.velocity = proj.velocity + env.gravity + env.wind;

  return result; 
}

int main() {

  /* Projectile p(point(0., 1., 0.), normalize(vector(1., 1., 0.))); */
  Projectile p(point(0., 1., 0.), vector(1., 1., 0.).normalize());
  Environment e(vector(0., -0.1, 0.), vector(-0.01, 0., 0.));

  
  int n = 0;
  
  std::cout << p.position << std::endl;

  /* while (p.position.y >= 0) { */
  while (p.position[1] >= 0) {
    p = tick(e, p); 
    std::cout << p.position;
    std::cout << ", " << p.velocity << std::endl;
    n++;
  }

  std::cout << std::endl;
  std::cout << "n: " << n;
  std::cout  << " position: ";
  std::cout  << p.position << std::endl;

  return 0;
}
