#include "object.h"
/* #include "intersection.h" */

namespace ray_tracer {

  std::vector<Intersection> Object::intersect(const Ray& r) const {
    Ray local_ray = r.transform(inverse(this->get_transform()));
    return local_intersect(local_ray); 
  }

  Tuple Object::normal_at(const Tuple& point) const {
    Tuple result;

    Tuple local_point = inverse(get_transform()) * point;
    Tuple local_normal = local_normal_at(local_point);

    result = transpose(inverse(get_transform())) * local_normal;
    result[3] = 0;
    result = result.normalize();
    return result;
  }
}
