#include "common.h"
#include "gradient_pattern.h"

namespace ray_tracer {

  Color GradientPattern::pattern_at(const Tuple& point) const {
    assert(point.is_point());
    Color distance = b - a;
    double fraction = point[0] - std::floor(point[0]);

    return a + distance * fraction;
  }
  
} 
