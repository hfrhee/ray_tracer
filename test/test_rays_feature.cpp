#include <iostream>
#include "gtest/gtest.h"
#include "ray.h"
#include "transform.h"

using namespace ray_tracer;

TEST(rays_feature, create_query_ray)
{
  Tuple origin = point(1., 2., 3.);
  Tuple direction = vector(4., 5., 6.);

  Ray ray(origin, direction);

  ASSERT_TRUE(ray.origin == origin);
  ASSERT_TRUE(ray.direction == direction);
}

TEST(rays_feature, compute_point_from_distance)
{
  Ray ray(point(2., 3., 4.), vector(1., 0., 0.));

  ASSERT_TRUE(ray.position(0.) == point(2., 3., 4.));
  ASSERT_TRUE(ray.position(1.) == point(3., 3., 4.));
  ASSERT_TRUE(ray.position(-1.) == point(1., 3., 4.));
  ASSERT_TRUE(ray.position(2.5) == point(4.5, 3., 4.));
}

TEST(rays_feature, translate_ray)
{
  Ray r(point(1., 2., 3.), vector(0., 1., 0.));

  Matrix m = translation(3., 4., 5.);

  Ray result = r.transform(m);

  ASSERT_TRUE(result.origin == point(4., 6., 8.));
  ASSERT_TRUE(result.direction == vector(0., 1., 0.));
}

TEST(rays_feature, scale_ray)
{
  Ray r(point(1., 2., 3.), vector(0., 1., 0.));

  Matrix m = scaling(2., 3., 4.);

  Ray result = r.transform(m);

  ASSERT_TRUE(result.origin == point(2, 6., 12.));
  ASSERT_TRUE(result.direction == vector(0., 3., 0.));
}
