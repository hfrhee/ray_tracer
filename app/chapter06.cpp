#include <iostream>
#include <fstream>
#include "common.h"
#include "canvas.h"
#include "transform.h"
#include "sphere.h"
#include "intersection.h"

using namespace ray_tracer;

int main() {

  Tuple ray_origin = point(0., 0., -5.);
  Ray ray(ray_origin, vector(0., 0., 0.));

  int wall_z = 10;

  double wall_size = 7.0;

  /* int canvas_pixels = 100; */
  int canvas_pixels = 500;

  double pixel_size = wall_size / canvas_pixels;

  double half = wall_size / 2;


  Canvas canvas(canvas_pixels, canvas_pixels);
  Color color = Color(1., 0., 0.);
  const auto s = Sphere::create();

  /* s->set_transform(scaling(1., 0.5, 1)); */
  /* s->set_transform(rotation_z( pi()/4 ) * scaling(0.5, 1, 1)); */
  /* s->set_transform(shearing(1., 0., 0., 0., 0., 0.) * scaling(0.5, 1, 1)); */

  Material m;
  m.set_color(Color(1., 0.2, 1.));
  s->set_material(m);

  Tuple light_position = point(-10., 10., -10.);
  Color light_color = Color(1., 1., 1.);
  Light light(light_position, light_color);

  for (int y=0; y<canvas_pixels; ++y) {
    double world_y = half - pixel_size * y;

    for (int x=0; x<canvas_pixels; ++x) {
      double world_x = -half + pixel_size * x; 

      Tuple position = point(world_x, world_y, wall_z);

      ray.direction = (position - ray_origin).normalize();

      std::vector<Intersection> xs = s->intersect(ray);

      if( hit(xs).has_value() ) {
        Tuple point = ray.position( hit(xs)->get_t() );
        Tuple normal = hit(xs)->get_object()->normal_at(point);

        Tuple eye = -ray.direction;
        
        color = hit(xs)->get_object()->get_material().lighting(*hit(xs)->get_object(), light, point, eye, normal, false);

        canvas.write_pixel(x, y, color);
      }

    }
  } 

  std::ofstream img("img06.ppm");
  canvas.to_ppm(img);
  img.close();

  return 0;
}
