#pragma once
#include <memory>
#include "color.h"
#include "light.h"
/* #include "pattern.h" */

namespace ray_tracer {

  class Pattern;
  class Object;

  class Material {
    public:
      Material() 
        : color(Color(1., 1., 1.)),
        ambient(0.1),
        diffuse(0.9),
        specular(0.9),
        shininess(200.0),
        reflective(0.),
        transparency(0.),
        refractive_index(1.),
        pattern(nullptr) {}

      Material(const Color& color,
          double ambient,
          double diffuse,
          double specular,
          double shininess,
          double reflective)
        : color(color),
        ambient(ambient),
        diffuse(diffuse),
        specular(specular),
        shininess(shininess),
        reflective(reflective),
        transparency(0.),
        refractive_index(1.),
        pattern(nullptr) {}
      
      Material& operator=(const Material& other) = default;
      bool operator==(const Material& other) const;

      Color get_color() const { return color; }
      double get_ambient() const { return ambient; }
      double get_diffuse() const { return diffuse; }
      double get_specular() const { return specular; }
      double get_shininess() const { return shininess; }
      double get_reflective() const { return reflective; }
      double get_transparency() const { return transparency; };
      double get_refractive_index() const { return refractive_index; };

      void set_color(const Color& other) { color = other; }
      void set_ambient(const double& other) { ambient = other; }
      void set_diffuse(const double& other) { diffuse = other; }
      void set_specular(const double& other) { specular = other; }
      void set_shininess(const double& other) { shininess = other; }
      void set_reflective(const double& other) { reflective = other; }
      void set_pattern(std::shared_ptr<Pattern> other) { pattern = std::move(other); }
      void set_transparency(const double& other) { transparency = other; };
      void set_refractive_index(const double& other) {  refractive_index = other; };

      Color lighting(const Object& object, const Light& light, const Tuple& point, const Tuple& eyev, const Tuple& normalv, bool in_shadow) const;

    private:
     Color color; 
     double ambient;
     double diffuse;
     double specular;
     double shininess;
     double reflective;
     double transparency;
     double refractive_index;
     std::shared_ptr<Pattern> pattern; // TODO make unique_ptr
  };

}
