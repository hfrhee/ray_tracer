#pragma once
#include <vector>
#include <memory>
#include "object.h"
#include "light.h"

namespace ray_tracer {
  
  class World {
    public:
      const static int MAX_RECURSION = 5;
      using vector_obj = std::vector<std::shared_ptr<Object>>;
      using vector_light = std::vector<Light>;

      World() = default;
      World(const World&) = default;
      World(World&&) = default;

      World(const vector_obj& objects, const vector_light& lights)
        : objects(objects), lights(lights) {}

      World& operator=(const World& other) = default;

      vector_obj get_objects() const;
      vector_light get_lights() const;

      void add_object(std::shared_ptr<Object> obj);
      void set_light(const Light&);
      void clear_lights();
      bool contains(const Object& obj) const;
      static World default_world();

      std::vector<Intersection> intersect(const Ray& ray) const;

      Color shade_hit(const Comps& comps, const int remaining=MAX_RECURSION) const;
      Color color_at(const Ray& ray, const int remaining=MAX_RECURSION) const;

      bool is_shadowed(const Tuple& point) const;

      Color reflected_color(const Comps& comps, const int remaining=MAX_RECURSION) const;

    private:
      vector_obj objects;
      vector_light lights;

  };

}
