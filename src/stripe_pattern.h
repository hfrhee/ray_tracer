#pragma once
#include "pattern.h"

namespace ray_tracer {

  class StripePattern : public Pattern {
    public:
      StripePattern(const Color& a, const Color& b) : a(a), b(b) {}

      Color get_a() const { return a; }
      Color get_b() const { return b; }

      virtual Color pattern_at(const Tuple& point) const override;

    private:
      Color a;
      Color b;
  };

}
