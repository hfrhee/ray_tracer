#include "transform.h"

namespace ray_tracer {


  Matrix translation(float x, float y, float z) {
    Matrix result = identity(4);

    result[0][3] = x;
    result[1][3] = y;
    result[2][3] = z;

    return result;
  }

  Matrix scaling(float x, float y, float z) {
    Matrix result = identity(4);

    result[0][0] = x;
    result[1][1] = y;
    result[2][2] = z;

    return result;
  }

  // r in radians
  Matrix rotation_x(float r) {
    Matrix result = identity(4);

    result[1][1] = std::cos(r);
    result[1][2] = -std::sin(r);
    result[2][1] = std::sin(r);
    result[2][2] = std::cos(r);

    return result;
  }

  Matrix rotation_y(float r) {
    Matrix result = identity(4);

    result[0][0] = std::cos(r);
    result[0][2] = std::sin(r);
    result[2][0] = -std::sin(r);
    result[2][2] = std::cos(r);

    return result;
  }

  Matrix rotation_z(float r) {
    Matrix result = identity(4);

    result[0][0] = std::cos(r);
    result[0][1] = -std::sin(r);
    result[1][0] = std::sin(r);
    result[1][1] = std::cos(r);

    return result;
  }

  Matrix shearing(float x_y, float x_z, float y_x, float y_z, float z_x, float z_y) {
    Matrix result = identity(4);

    result[0][1] = x_y;
    result[0][2] = x_z;
    result[1][0] = y_x;
    result[1][2] = y_z;
    result[2][0] = z_x;
    result[2][1] = z_y;

    return result;
  }

  /* Ray transform(const Ray& ray, const Matrix& matrix) { */
  /*   Ray result(point(0., 0., 0.), vector(0., 0., 0.)); */
  /*   result.origin = matrix * ray.origin; */
  /*   result.direction = matrix * ray.direction; */

  /*   return result; */
  /* } */

  Matrix view_transform(const Tuple& from, const Tuple& to, const Tuple& up) {
    Matrix result = identity(4);

    Tuple forward = (to - from).normalize();
    Tuple upn = up.normalize();
    Tuple left = cross(forward, upn);
    Tuple true_up = cross(left, forward);

    result[0][0] = left[0];
    result[0][1] = left[1];
    result[0][2] = left[2];
    /* result[0][3] = 0.; */

    result[1][0] = true_up[0];
    result[1][1] = true_up[1];
    result[1][2] = true_up[2];
    /* result[1][3] = 0.; */

    result[2][0] = -forward[0];
    result[2][1] = -forward[1];
    result[2][2] = -forward[2];
    /* result[2][3] = 0.; */

    result = result * translation(-from[0], -from[1], -from[2]);

    return result;
  }
}
