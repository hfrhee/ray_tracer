#include "common.h"
#include "stripe_pattern.h"

namespace ray_tracer {

  Color StripePattern::pattern_at(const Tuple& point) const {
    assert(point.is_point());

    return static_cast<int>(std::floor(point[0])) % 2 ? b : a;
  }
  
} 
