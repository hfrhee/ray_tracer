#include "gtest/gtest.h"
#include "tuple.h"
#include "color.h"

using namespace ray_tracer;

TEST(tuples_feature, is_point)
{
    Tuple a(4.3, -4.2, 3.1, 1.0);
    ASSERT_EQ(a[Tuple::x], 4.3);
    ASSERT_EQ(a[Tuple::y], -4.2);
    ASSERT_EQ(a[Tuple::z], 3.1);
    ASSERT_EQ(a[Tuple::w], 1.0);
    ASSERT_TRUE(a.is_point());
    ASSERT_FALSE(a.is_vector());
}

TEST(tuples_feature, is_vector)
{
    Tuple a(4.3, -4.2, 3.1, 0.0);
    ASSERT_EQ(a[Tuple::x], 4.3);
    ASSERT_EQ(a[Tuple::y], -4.2);
    ASSERT_EQ(a[Tuple::z], 3.1);
    ASSERT_EQ(a[Tuple::w], 0.0);
    ASSERT_FALSE(a.is_point());
    ASSERT_TRUE(a.is_vector());
}

TEST(tuples_feature, create_point)
{
    Tuple a = point(4., -4., 3.);
    ASSERT_TRUE(a == Tuple(4., -4., 3., 1.));
    ASSERT_TRUE(a.is_point());
    ASSERT_FALSE(a.is_vector());
}

TEST(tuples_feature, create_vector)
{
    Tuple a = vector(4., -4., 3.);
    /* ASSERT_EQ(a.x, 4.); */
    /* ASSERT_EQ(a.y, -4.); */
    /* ASSERT_EQ(a.z, 3.); */
    /* ASSERT_EQ(a.w, 0.); */
    ASSERT_TRUE(a == Tuple(4., -4., 3., 0.));
    ASSERT_FALSE(a.is_point());
    ASSERT_TRUE(a.is_vector());
}

TEST(tuples_feature, add)
{
  Tuple a = Tuple(3., -2., 5., 1.);
  Tuple b = Tuple(-2., 3., 1., 0.);
  Tuple result = a + b;
  ASSERT_TRUE(result == Tuple(1., 1., 6., 1.));
}

TEST(tuples_feature, subtract_points)
{
  Tuple a = point(3., 2., 1.);
  Tuple b = point(5., 6., 7.);
  Tuple result = a - b;
  ASSERT_TRUE(result == vector(-2., -4., -6.));
}

TEST(tuples_feature, subtract_vector_from_point)
{
  Tuple a = point(3., 2., 1.);
  Tuple b = vector(5., 6., 7.);
  Tuple result = a - b;
  ASSERT_TRUE(result == point(-2., -4., -6.));
}

TEST(tuples_feature, subtract_vectors)
{
  Tuple a = vector(3., 2., 1.);
  Tuple b = vector(5., 6., 7.);
  Tuple result = a - b;
  ASSERT_TRUE(result == vector(-2., -4., -6.));
}


TEST(tuples_feature, subtract_vector_from_zero)
{
  Tuple zero = vector(0., 0., 0.);
  Tuple v = vector(1., -2., 3.);
  Tuple result = zero - v;
  ASSERT_TRUE(result == vector(-1., 2., -3.));
}

TEST(tuples_feature, negate)
{
  Tuple a(1., -2, 3., -4.);
  ASSERT_TRUE(-a == Tuple(-1., 2., -3., 4.));
}

TEST(tuples_feature, scalar_multiplication)
{
  Tuple a(1., -2., 3., -4.);
  Tuple result = a * 3.5;
  ASSERT_TRUE(result == Tuple(3.5, -7., 10.5, -14.));
}

TEST(tuples_feature, fraction_multiplication)
{
  Tuple a(1., -2., 3., -4.);
  Tuple result = a * .5;
  ASSERT_TRUE(result == Tuple(0.5, -1., 1.5, -2.));
}

TEST(tuples_feature, scalar_division)
{
  Tuple a(1., -2., 3., -4.);
  Tuple result = a / 2;
  ASSERT_TRUE(result == Tuple(0.5, -1., 1.5, -2.));
}


TEST(tuples_feature, magnitude_1)
{
  Tuple v = vector(1., 0., 0.);
  double result = magnitude(v);
  ASSERT_EQ(result, 1.);
}

TEST(tuples_feature, magnitude_2)
{
  Tuple v = vector(0., 1., 0.);
  double result = magnitude(v);
  ASSERT_EQ(result, 1.);
}

TEST(tuples_feature, magnitude_3)
{
  Tuple v = vector(0., 0., 1.);
  double result = magnitude(v);
  ASSERT_EQ(result, 1.);
}

TEST(tuples_feature, magnitude_4)
{
  Tuple v = vector(1., 2., 3.);
  double result = magnitude(v);
  ASSERT_NEAR(result, std::sqrt(14), 1e-5);
}

TEST(tuples_feature, magnitude_5)
{
  Tuple v = vector(-1., -2., -3.);
  double result = magnitude(v);
  ASSERT_NEAR(result, std::sqrt(14), 1e-5);
}


TEST(tuples_feature, normalize_1)
{
  Tuple v = vector(4., 0., 0.);
  /* Tuple result = normalize(v); */
  Tuple result = v.normalize();
  ASSERT_TRUE(result == vector(1., 0., 0.));
}

TEST(tuples_feature, normalize_2)
{
  Tuple v = vector(1., 2., 3.);
  /* Tuple result = normalize(v); */
  Tuple result = v.normalize();
  double abs = std::sqrt(14);
  ASSERT_TRUE(result == vector(1./abs, 2./abs, 3./abs));
}

TEST(tuples_feature, normalize_3)
{
  Tuple v = vector(1., 2., 3.);
  /* Tuple result = normalize(v); */
  Tuple result = v.normalize();
  ASSERT_EQ(magnitude(result), 1.);
}

TEST(tuples_feature, dot_product)
{
  Tuple a = vector(1., 2., 3.);
  Tuple b = vector(2., 3., 4.);
  double result = dot(a,b);
  ASSERT_EQ(result, 20.);
}

TEST(tuples_feature, cross_product)
{
  Tuple a = vector(1., 2., 3.);
  Tuple b = vector(2., 3., 4.);
  ASSERT_TRUE(cross(a,b) == vector(-1., 2., -1.));
  ASSERT_TRUE(cross(b,a) == vector(1., -2., 1.));
}

TEST(tuples_feature, color)
{
  Color c = Color(-0.5, 0.4, 1.7);
  ASSERT_TRUE(c[Color::red] == -.5);
  ASSERT_TRUE(c[Color::green] == .4);
  ASSERT_TRUE(c[Color::blue] == 1.7);
}

TEST(tuples_feature, color_add)
{
  Color c1 = Color(.9, .6, .75);
  Color c2 = Color(.7, .1, .25);
  Color result = c1 + c2;
  ASSERT_TRUE(result == Color(1.6, .7, 1.));
}

TEST(tuples_feature, color_subtract)
{
  Color c1 = Color(.9, .6, .75);
  Color c2 = Color(.7, .1, .25);
  Color result = c1 - c2;
  ASSERT_TRUE(result == Color(0.2, .5, .5));
}

TEST(tuples_feature, color_scalar)
{
  Color c = Color(.2, .3, .4);
  Color result = c * 2;
  ASSERT_TRUE(result == Color(0.4, .6, .8));
}

TEST(tuples_feature, color_mult)
{
  Color c1 = Color(1., .2, .4);
  Color c2 = Color(.9, 1., .1);
  Color result = hadamard_product(c1, c2);
  ASSERT_TRUE(result == Color(0.9, .2, .04));
}

TEST(tuples_feature, reflect_45)
{
  Tuple v = vector(1., -1., 0.);
  Tuple n = vector(0., 1., 0.);

  Tuple result = v.reflect(n);
  ASSERT_TRUE(result == vector(1., 1., 0.));
}

TEST(tuples_feature, reflect_slanted_surface)
{
  Tuple v = vector(0., -1., 0.);
  Tuple n = vector(std::sqrt(2)/2, std::sqrt(2)/2., 0.);

  Tuple result = v.reflect(n);
  ASSERT_TRUE(result == vector(1., 0., 0.));
}





