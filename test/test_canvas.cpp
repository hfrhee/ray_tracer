#include <iostream>
#include <sstream>
#include <string>

#include "gtest/gtest.h"
#include "color.h"
#include "canvas.h"


using namespace ray_tracer;

TEST(canvas_feature, create_canvas)
{
    Canvas c(10, 20);
    ASSERT_TRUE(c.width == 10);
    ASSERT_TRUE(c.height == 20);

    for (int y=0; y < c.height; ++y) {
      for (int x=0; x < c.width; ++x) {
        /* ASSERT_TRUE( c.pixel[row][col] == Color(0., 0., 0.)); */
        ASSERT_TRUE( c.pixel_at(x,y) == Color(0., 0., 0.));
      }
    }
}

TEST(canvas_feature, create_canvas_2)
{
    Canvas c(900, 550);
    ASSERT_TRUE(c.width == 900);
    ASSERT_TRUE(c.height == 550);

    for (int y=0; y < c.height; ++y) {
      for (int x=0; x < c.width; ++x) {
        /* ASSERT_TRUE( c.pixel[row][col] == Color(0., 0., 0.)); */
        ASSERT_TRUE( c.pixel_at(x,y) == Color(0., 0., 0.));
      }
    }
}

TEST(canvas_feature, write_canvas)
{
    Canvas c(10,20);
    Color red(1., 0., 0.);
    c.write_pixel(2,3,red);
    ASSERT_TRUE( c.pixel_at(2,3) == red);
}

TEST(canvas_feature, write_PPM_header)
{
    Canvas c(5,3);
    std::stringstream ss;
    c.to_ppm(ss);
    std::string result = ss.str();
    std::string expected = "P3\n5 3\n255\n 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n";
    ASSERT_STREQ(expected.c_str(), result.c_str());
}

TEST(canvas_feature, PPM_pixel_data)
{
    Canvas c(5,3);
    Color c1 = Color(1.5, 0., 0.);
    Color c2 = Color(.0, .5, 0.);
    Color c3 = Color(-.5, 0., 1.);

    c.write_pixel(0, 0, c1);
    c.write_pixel(2, 1, c2);
    c.write_pixel(4, 2, c3);

    std::stringstream ss;
    c.to_ppm(ss);
    std::string result = ss.str();
    std::string expected = "P3\n5 3\n255\n 255 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n 0 0 0 0 0 0 0 128 0 0 0 0 0 0 0\n 0 0 0 0 0 0 0 0 0 0 0 0 0 0 255\n";
    ASSERT_STREQ(expected.c_str(), result.c_str());
}

TEST(canvas_feature, PPM_70_characters)
{
  Canvas c(10,2);
  for (int y=0; y<2; ++y) {
    for (int x=0; x<10; ++x) {
      c.write_pixel(x,y, Color(1., .8, .6));
    }
  }

  /* c.to_ppm(std::cout); */
  std::stringstream ss;
  c.to_ppm(ss);
  std::string result = ss.str();
  std::string expected = "P3\n10 2\n255\n 255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n153 255 204 153 255 204 153 255 204 153 255 204 153\n 255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n153 255 204 153 255 204 153 255 204 153 255 204 153\n";
  ASSERT_STREQ(expected.c_str(), result.c_str());

}

/* TEST(canvas_feature, PPM_end_newline) */
/* { */
/* } */




