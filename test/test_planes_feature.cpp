#include <iostream>
#include "gtest/gtest.h"
#include "plane.h"

using namespace ray_tracer;

TEST(planes_feature, normal_of_plane_is_constant_everywhere) {
  const auto p = Plane::create();

  Tuple n1 = p->local_normal_at(point(0., 0., 0.));
  Tuple n2 = p->local_normal_at(point(10., 0., -10.));
  Tuple n3 = p->local_normal_at(point(-5., 0., 150.));

  ASSERT_TRUE(n1 == vector(0., 1., 0.));
  ASSERT_TRUE(n2 == vector(0., 1., 0.));
  ASSERT_TRUE(n3 == vector(0., 1., 0.));
}

TEST(planes_feature, intersect_with_ray_parallel_to_plane) {
  const auto p = Plane::create();
  Ray r(point(0., 10., 0.), vector(0., 0., 1.));

  const auto xs = p->local_intersect(r);

  ASSERT_TRUE(xs.empty());
}

TEST(planes_feature, intersect_with_coplanar_ray) {
  const auto p = Plane::create();
  Ray r(point(0., 0., 0.), vector(0., 0., 1.));

  const auto xs = p->local_intersect(r);

  ASSERT_TRUE(xs.empty());
}

TEST(planes_feature, intersect_plane_from_above) {
  const auto p = Plane::create();
  Ray r(point(0., 1., 0.), vector(0., -1., 0.));

  const auto xs = p->local_intersect(r);

  ASSERT_TRUE(xs.size() == 1);
  ASSERT_TRUE(xs[0].get_t() == 1.);
  ASSERT_STREQ(xs[0].get_object()->get_name().c_str(), "Plane");
}

TEST(planes_feature, intersect_plane_from_below) {
  const auto p = Plane::create();
  Ray r(point(0., -1., 0.), vector(0., 1., 0.));

  const auto xs = p->local_intersect(r);

  ASSERT_TRUE(xs.size() == 1);
  ASSERT_TRUE(xs[0].get_t() == 1.);
  ASSERT_STREQ(xs[0].get_object()->get_name().c_str(), "Plane");
}
