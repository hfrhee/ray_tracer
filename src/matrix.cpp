#include "matrix.h"


namespace ray_tracer {

ray_tracer::Matrix operator*(const ray_tracer::Matrix& lhs, const ray_tracer::Matrix& rhs) {
    assert(lhs.num_cols() == rhs.num_rows());

    size_t nrows = lhs.num_rows();
    size_t ncols = rhs.num_cols();

    ray_tracer::Matrix result(nrows, ncols);

#pragma omp parallel for
    for (size_t r=0; r<nrows; ++r) {
      for(size_t c=0; c<ncols; ++c) {
        for (size_t n=0; n<lhs.num_cols(); ++n) {
          result[r][c] += lhs[r][n] * rhs[n][c];
        }
      }
    }

    return result;
  }

  ray_tracer::Tuple operator*(const ray_tracer::Matrix& mat, const ray_tracer::Tuple& tup) {
    ray_tracer::Tuple result;
    
    assert(mat.num_cols()==ray_tracer::Tuple::N);

    for(size_t idx=0; idx<ray_tracer::Tuple::N; ++idx) {
      for(size_t c=0; c<mat.num_cols(); ++c) {
        result[idx] += mat[idx][c] * tup[c];
      }
    }

    return result;
  }

  ray_tracer::Matrix identity(size_t n) {
    ray_tracer::Matrix result(n, n);

    for (size_t r=0; r<n; ++r) {
      for (size_t c=0; c<n; ++c) {
        if (r==c) {
          result[r][c] = 1.;
        } else {
          result[r][c] = 0.;
        }
      }
    }

    return result;
  }

  ray_tracer::Matrix transpose(const ray_tracer::Matrix& mat) {
    ray_tracer::Matrix result(mat.num_rows(), mat.num_cols());

    for (size_t r=0; r<mat.num_rows(); ++r) {
      for (size_t c=0; c<mat.num_cols(); ++c) {
        result[r][c] = mat[c][r];
      }
    }

    return result;
  }


  ray_tracer::Matrix submatrix(const ray_tracer::Matrix& mat, size_t row, size_t col) {
    assert(row < mat.num_rows());
    assert(col < mat.num_cols());

    size_t nrows = mat.num_rows()-1;
    size_t ncols = mat.num_cols()-1;

    ray_tracer::Matrix result(nrows, ncols);

    size_t res_r = 0;
    size_t res_c = 0;

    for (size_t mat_r=0; mat_r<mat.num_rows(); ++mat_r) {
      res_c = 0;
      if (mat_r == row) continue;

      for (size_t mat_c=0; mat_c<mat.num_cols(); ++mat_c) {

        if (mat_c == col) { 
          continue;
        }
        else {
          result[res_r][res_c] = mat[mat_r][mat_c];
          res_c++;
        } 
      }
      res_r++;
    }

    return result;
  }

  float det_2x2(const ray_tracer::Matrix& mat) {
    assert(mat.num_rows() == 2);
    assert(mat.num_cols() == 2);

    return mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
  }

  float det(const ray_tracer::Matrix& mat);

  float mat_minor(const ray_tracer::Matrix& mat, size_t row, size_t col) {
    return det(submatrix(mat, row, col));
  }

  float cofactor(const ray_tracer::Matrix& mat, size_t row, size_t col) {
    float result = mat_minor(mat, row, col);

    if ((row+col)%2 != 0) {
      result = -1 * result; 
    }

    return result;
  }

  float det(const ray_tracer::Matrix& mat) {
    assert(mat.num_rows() ==  mat.num_cols());
    assert(mat.num_rows() != 0);

    float result = 0;

    if (mat.num_rows() == 1 ) {
      return mat[0][0];
    }

    if (mat.num_rows() == 2) {
      return det_2x2(mat);
    }

    for (size_t c=0; c<mat.num_cols(); ++c) {
      result = result + mat[0][c] * cofactor(mat, 0, c);
    }

    return result;
  }

  ray_tracer::Matrix inverse(const ray_tracer::Matrix& mat) {
    assert(mat.num_rows() == mat.num_cols());

    float det_mat = det(mat);
    assert(det_mat);  
    
    ray_tracer::Matrix result(mat.num_rows(), mat.num_cols());

    for (size_t row=0; row<mat.num_rows(); ++row) {
      for (size_t col=0; col<mat.num_cols(); ++col) {
        result[col][row] = cofactor(mat, row, col) / det_mat;
      }
    }

    return result;
  }

  std::ostream& operator<<(std::ostream& os, const ray_tracer::Matrix& A) {
    for (size_t r=0; r<A.num_rows(); ++r) {
      os << "[";
      for (size_t c=0; c<A.num_cols(); ++c) {
        os << A[r][c] << (c+1 < A.num_cols() ? " " : "");
      }
      os << "]\n";
    }

    return os;
  }
}
