#pragma once

#include <vector>
#include <iostream>
#include <memory>
#include <optional>

#include "common.h"
#include "ray.h"
/* #include "object.h" */

namespace ray_tracer {

  class Object;

  struct Comps {
    double t;
    // TODO change to unique_ptr
    std::shared_ptr<const Object> object;
    Tuple point;
    Tuple eyev;
    Tuple normalv;
    bool inside;
    Tuple over_point;
    Tuple reflectv;
    double n1;
    double n2;
  };

  class Intersection {
    public:
      // TODO change to unique_ptr
      Intersection(const double& t, const std::shared_ptr<const Object>& object)
        : t(t), object(object) {}

      Intersection(const Intersection& other) = default;
      Intersection(Intersection&& other) = default;
      Intersection& operator=(const Intersection& other) = default;

      bool operator==(const Intersection& other) const;
      bool operator!=(const Intersection& other) const;

      double get_t() const { return t; }
      // TODO change to unique_ptr
      const std::shared_ptr<const Object>& get_object() const { return object; }

      Comps prepare_computations(const Ray& r) const; 
      Comps zz_prepare_computations(const Ray& r,
          const std::vector<Intersection>& xs) const; 

    private:
      double t;
      // TODO change to unique_ptr
      std::shared_ptr<const Object> object;
  };

  std::optional<const Intersection> hit(const std::vector<Intersection>& xs);

}
