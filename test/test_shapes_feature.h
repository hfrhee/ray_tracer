#pragma once
#include <vector>
#include "tuple.h"
#include "ray.h"
#include "object.h"

namespace ray_tracer{

  class Test_Shape : public Object {
    public:
      Ray saved_ray;

      Test_Shape() : Object("Test_Shape") {}
      /* static std::shared_ptr<Test_Shape> create() { */
      /*   return std::make_shared<Test_Shape>(Test_Shape()); */ 
      /* } */
  
      virtual std::vector<Intersection> local_intersect(const Ray& r_in) const override {
        assert(r_in.origin.is_point());

        // abuse of const_cast to allow testing
        const_cast<Test_Shape*>(this)->saved_ray = r_in;
          
        return {}; 
      }
      
      virtual Tuple local_normal_at(const Tuple& point_in) const override {
        assert(point_in.is_point());
        
        Tuple result = vector(point_in[0], point_in[1], point_in[2]);
        return result;
      }
 
    private:

      virtual bool doCompare(const Object& other) const override {
        const auto *s = dynamic_cast<const Test_Shape*>(&other); 
        assert(s != nullptr);
        return true;
      }
  };
}

