#include "common.h"
#include "checkers_pattern.h"

namespace ray_tracer {

  Color CheckersPattern::pattern_at(const Tuple& point) const {
    assert(point.is_point());

    int tmp_x = static_cast<int>(std::floor(point[0])); 
    int tmp_y = static_cast<int>(std::floor(point[1])); 
    int tmp_z = static_cast<int>(std::floor(point[2])); 

    return (tmp_x + tmp_y + tmp_z) % 2 ? b : a;
  }
  
} 
