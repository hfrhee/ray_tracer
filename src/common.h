#pragma once

#include <cmath>
#include <cassert>

#define EPSILON 1e-4
#define ALMOST_EQUAL(x, y) (std::abs((x) -(y))<EPSILON)

constexpr double pi() { return std::atan(1)*4; }
