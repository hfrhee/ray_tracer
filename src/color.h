#pragma once

#include "tuple.h"

namespace ray_tracer {

  class Color : public Tuple {
    public:
      enum {red, green, blue};
      static const int max_value = 255;

      Color() : Tuple(0., 0., 0., 0.) {}

      Color(const Tuple& tuple)
        : Tuple(tuple[0], tuple[1], tuple[2], tuple[3]) {}

      Color(double red, double green, double blue)
        : Tuple(red, green, blue, 0.) {}
  };

  Color hadamard_product(const Color& c1, const Color& c2);

  namespace color {
    static const Color black(0., 0., 0.);
    static const Color white(1., 1., 1.);
  }
}

