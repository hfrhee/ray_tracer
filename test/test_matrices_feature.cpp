#include <iostream>

#include "gtest/gtest.h"
#include "matrix.h"

using namespace ray_tracer;

TEST(matrices_feature, constructing_4x4_matrix)
{
    Matrix m(4, 4);
    ASSERT_TRUE(m.num_rows() == 4);
    ASSERT_TRUE(m.num_cols() == 4);

    m(0,0) = 1.;
    m(0,1) = 2.;
    m(0,2) = 3.;
    m(0,3) = 4.;

    m(1,0) = 5.5;
    m(1,1) = 6.5;
    m(1,2) = 7.5;
    m(1,3) = 8.5;

    m(2,0) = 9.;
    m(2,1) = 10.;
    m(2,2) = 11.;
    m(2,3) = 12.;

    m(3,0) = 13.5;
    m(3,1) = 14.5;
    m(3,2) = 15.5;
    m(3,3) = 16.5;

    ASSERT_TRUE(m[0][0] == 1.);
    ASSERT_TRUE(m[0][3] == 4.);
    ASSERT_TRUE(m[1][0] == 5.5);
    ASSERT_TRUE(m[1][2] == 7.5);
    ASSERT_TRUE(m[2][2] == 11.);
    ASSERT_TRUE(m[3][0] == 13.5);
    ASSERT_TRUE(m[3][2] == 15.5);
}

TEST(matrices_feature, constructing_2x2_matrix)
{
    Matrix m(2, 2);
    ASSERT_TRUE(m.num_rows() == 2);
    ASSERT_TRUE(m.num_cols() == 2);

    m(0,0) = -3.;
    m(0,1) = 5.;

    m(1,0) = 1.;
    m(1,1) = -2.;

    ASSERT_TRUE(m[0][0] == -3.);
    ASSERT_TRUE(m[0][1] == 5.);
    ASSERT_TRUE(m[1][0] == 1.);
    ASSERT_TRUE(m[1][1] == -2.);
}

TEST(matrices_feature, constructing_3x3_matrix)
{
    Matrix m(3, 3);
    ASSERT_TRUE(m.num_rows() == 3);
    ASSERT_TRUE(m.num_cols() == 3);

    m(0,0) = -3.;
    m(0,1) = 5.;
    m(0,2) = 0.;

    m(1,0) = 1.;
    m(1,1) = -2.;
    m(1,2) = -7.;

    m(2,0) = 0.;
    m(2,1) = 1.;
    m(2,2) = 1.;

    ASSERT_TRUE(m[0][0] == -3.);
    ASSERT_TRUE(m[1][1] == -2.);
    ASSERT_TRUE(m[2][2] == 1.);
}

TEST(matrices_feature, matrix_equality)
{
    Matrix A(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);

    A(0,0) = 1.;
    A(0,1) = 2.;
    A(0,2) = 3.;
    A(0,3) = 4.;

    A(1,0) = 5.;
    A(1,1) = 6.;
    A(1,2) = 7.;
    A(1,3) = 8.;

    A(2,0) = 9.;
    A(2,1) = 8.;
    A(2,2) = 7.;
    A(2,3) = 6.;

    A(3,0) = 5.;
    A(3,1) = 4.;
    A(3,2) = 3.;
    A(3,3) = 2.;

    Matrix B(A);

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */

    ASSERT_TRUE(A == B);
}

TEST(matrices_feature, matrix_inequality)
{
    Matrix A(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);

    A(0,0) = 1.;
    A(0,1) = 2.;
    A(0,2) = 3.;
    A(0,3) = 4.;

    A(1,0) = 5.;
    A(1,1) = 6.;
    A(1,2) = 7.;
    A(1,3) = 8.;

    A(2,0) = 9.;
    A(2,1) = 8.;
    A(2,2) = 7.;
    A(2,3) = 6.;

    A(3,0) = 5.;
    A(3,1) = 4.;
    A(3,2) = 3.;
    A(3,3) = 2.;

    Matrix B(4,4);
    B(0,0) = 2.;
    B(0,1) = 3.;
    B(0,2) = 4.;
    B(0,3) = 5.;

    B(1,0) = 6.;
    B(1,1) = 7.;
    B(1,2) = 8.;
    B(1,3) = 9.;

    B(2,0) = 8.;
    B(2,1) = 7.;
    B(2,2) = 6.;
    B(2,3) = 5.;

    B(3,0) = 4.;
    B(3,1) = 3.;
    B(3,2) = 2.;
    B(3,3) = 1.;

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */

    ASSERT_TRUE(A != B);
}

TEST(matrices_feature, matrix_assign)
{
    Matrix A(3, 3);
    ASSERT_TRUE(A.num_rows() == 3);
    ASSERT_TRUE(A.num_cols() == 3);

    A(0,0) = -3.;
    A(0,1) = 5.;
    A(0,2) = 0.;

    A(1,0) = 1.;
    A(1,1) = -2.;
    A(1,2) = -7.;

    A(2,0) = 0.;
    A(2,1) = 1.;
    A(2,2) = 1.;

    Matrix B(3,3);
    
    B = A;

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */

    ASSERT_TRUE(A == B);
}

TEST(matrices_feature, matrix_multiplication)
{
    Matrix A(4, 4);
    Matrix B(4, 4);
    Matrix C(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);
    ASSERT_TRUE(B.num_rows() == 4);
    ASSERT_TRUE(B.num_rows() == 4);
    ASSERT_TRUE(C.num_cols() == 4);
    ASSERT_TRUE(C.num_cols() == 4);

    A(0,0) = 1.;
    A(0,1) = 2.;
    A(0,2) = 3.;
    A(0,3) = 4.;

    A(1,0) = 5.;
    A(1,1) = 6.;
    A(1,2) = 7.;
    A(1,3) = 8.;

    A(2,0) = 9.;
    A(2,1) = 8.;
    A(2,2) = 7.;
    A(2,3) = 6.;

    A(3,0) = 5.;
    A(3,1) = 4.;
    A(3,2) = 3.;
    A(3,3) = 2.;

    B(0,0) = -2.;
    B(0,1) = 1.;
    B(0,2) = 2.;
    B(0,3) = 3.;

    B(1,0) = 3.;
    B(1,1) = 2.;
    B(1,2) = 1.;
    B(1,3) = -1.;

    B(2,0) = 4.;
    B(2,1) = 3.;
    B(2,2) = 6.;
    B(2,3) = 5.;

    B(3,0) = 1.;
    B(3,1) = 2.;
    B(3,2) = 7.;
    B(3,3) = 8.;

    C(0,0) = 20.;
    C(0,1) = 22.;
    C(0,2) = 50.;
    C(0,3) = 48.;

    C(1,0) = 44.;
    C(1,1) = 54.;
    C(1,2) = 114.;
    C(1,3) = 108.;

    C(2,0) = 40.;
    C(2,1) = 58.;
    C(2,2) = 110.;
    C(2,3) = 102.;

    C(3,0) = 16.;
    C(3,1) = 26.;
    C(3,2) = 46.;
    C(3,3) = 42.;

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */
    /* std::cout << C << std::endl; */

    Matrix result = A * B;

    /* std::cout << result << std::endl; */

    ASSERT_TRUE(C == result);
}

TEST(matrices_feature, matrix_tuple_multiplication)
{
    Matrix A(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);

    A(0,0) = 1.;
    A(0,1) = 2.;
    A(0,2) = 3.;
    A(0,3) = 4.;

    A(1,0) = 2.;
    A(1,1) = 4.;
    A(1,2) = 4.;
    A(1,3) = 2.;

    A(2,0) = 8.;
    A(2,1) = 6.;
    A(2,2) = 4.;
    A(2,3) = 1.;

    A(3,0) = 0.;
    A(3,1) = 0.;
    A(3,2) = 0.;
    A(3,3) = 1.;

    Tuple b(1., 2., 3., 1.);

    /* std::cout << A << std::endl; */
    /* std::cout << b << std::endl; */

    Tuple result = A * b;
    /* std::cout << result << std::endl; */
    Tuple expected(18., 24., 33., 1.);

    ASSERT_TRUE( result == expected );
}

TEST(matrices_feature, matrix_identity)
{
    Matrix A(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);

    A(0,0) = 0.;
    A(0,1) = 1.;
    A(0,2) = 2.;
    A(0,3) = 4.;

    A(1,0) = 1.;
    A(1,1) = 2.;
    A(1,2) = 4.;
    A(1,3) = 8.;

    A(2,0) = 2.;
    A(2,1) = 4.;
    A(2,2) = 8.;
    A(2,3) = 16.;

    A(3,0) = 4.;
    A(3,1) = 8.;
    A(3,2) = 16.;
    A(3,3) = 32.;


    /* std::cout << A << std::endl; */

    Matrix result = A * identity(4);

    ASSERT_TRUE( result == A );
}

TEST(matrices_feature, matrix_identity_tuple)
{
    Tuple a(1., 2., 3., 4.);
    
    Tuple result = identity(4) * a;

    ASSERT_TRUE( result == a );
}

TEST(matrices_feature, matrix_transpose)
{
    Matrix A(4, 4);
    ASSERT_TRUE(A.num_rows() == 4);
    ASSERT_TRUE(A.num_cols() == 4);

    A(0,0) = 0.;
    A(0,1) = 9.;
    A(0,2) = 3.;
    A(0,3) = 0.;

    A(1,0) = 9.;
    A(1,1) = 8.;
    A(1,2) = 0.;
    A(1,3) = 8.;

    A(2,0) = 1.;
    A(2,1) = 8.;
    A(2,2) = 5.;
    A(2,3) = 3.;

    A(3,0) = 0.;
    A(3,1) = 0.;
    A(3,2) = 5.;
    A(3,3) = 8.;
    
    Matrix C(4, 4);
    ASSERT_TRUE(C.num_rows() == 4);
    ASSERT_TRUE(C.num_cols() == 4);

    C(0,0) = 0.;
    C(0,1) = 9.;
    C(0,2) = 1.;
    C(0,3) = 0.;

    C(1,0) = 9.;
    C(1,1) = 8.;
    C(1,2) = 8.;
    C(1,3) = 0.;

    C(2,0) = 3.;
    C(2,1) = 0.;
    C(2,2) = 5.;
    C(2,3) = 5.;

    C(3,0) = 0.;
    C(3,1) = 8.;
    C(3,2) = 3.;
    C(3,3) = 8.;

    Matrix result = transpose(A);

    /* std::cout << A << std::endl; */
    /* std::cout << result << std::endl; */

    ASSERT_TRUE( result == C );
}

TEST(matrices_feature, matrix_transpose_identity)
{
    Matrix A = identity(4);
    Matrix result = transpose(A);
    ASSERT_TRUE( result == A );
}

TEST(matrices_feature, det_2x2_matrix)
{
    Matrix A(2,2);
    A(0,0) = 1.;
    A(0,1) = 5.;
    A(1,0) = -3.;
    A(1,1) = 2.;

    float result = det_2x2(A);
    ASSERT_TRUE( result == 17. );
}

TEST(matrices_feature, submatrix_3x3)
{
    Matrix A(3,3);
    A(0,0) = 1.;
    A(0,1) = 5.;
    A(0,2) = 0.;

    A(1,0) = -3.;
    A(1,1) = 2.;
    A(1,2) = 7.;

    A(2,0) = 0.;
    A(2,1) = 6.;
    A(2,2) = -3.;

    Matrix result = submatrix(A, 0, 2);

    Matrix B(2,2);

    B(0,0) = -3.;
    B(0,1) = 2.;

    B(1,0) = 0.;
    B(1,1) = 6.;
    /* std::cout << A << std::endl; */
    /* std::cout << submatrix(A, 0, 0) << std::endl; */
    /* std::cout << submatrix(A, 0, 1) << std::endl; */
    /* std::cout << submatrix(A, 0, 2) << std::endl; */
    /* std::cout << submatrix(A, 1, 0) << std::endl; */
    /* std::cout << submatrix(A, 1, 1) << std::endl; */
    /* std::cout << submatrix(A, 1, 2) << std::endl; */
    /* std::cout << submatrix(A, 2, 0) << std::endl; */
    /* std::cout << submatrix(A, 2, 1) << std::endl; */
    /* std::cout << submatrix(A, 2, 2) << std::endl; */
    ASSERT_TRUE( result == B );
}

TEST(matrices_feature, submatrix_4x4)
{
    Matrix A(4,4);

    A(0,0) = -6.;
    A(0,1) = 1.;
    A(0,2) = 1.;
    A(0,3) = 6.;

    A(1,0) = -8.;
    A(1,1) = 5.;
    A(1,2) = 8.;
    A(1,3) = 6.;

    A(2,0) = -1.;
    A(2,1) = 0.;
    A(2,2) = 8.;
    A(2,3) = 2.;

    A(3,0) = -7.;
    A(3,1) = 1.;
    A(3,2) = -1.;
    A(3,3) = 1.;

    Matrix B(3,3);

    B(0,0) = -6.;
    B(0,1) = 1.;
    B(0,2) = 6.;

    B(1,0) = -8.;
    B(1,1) = 8.;
    B(1,2) = 6.;

    B(2,0) = -7.;
    B(2,1) = -1.;
    B(2,2) = 1.;

    Matrix result = submatrix(A, 2, 1);

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */
    /* std::cout << submatrix(A, 2, 1) << std::endl; */
    /* std::cout << submatrix(A, 0, 1) << std::endl; */
    /* std::cout << submatrix(A, 0, 2) << std::endl; */
    /* std::cout << submatrix(A, 1, 0) << std::endl; */
    /* std::cout << submatrix(A, 1, 1) << std::endl; */
    /* std::cout << submatrix(A, 1, 2) << std::endl; */
    /* std::cout << submatrix(A, 2, 0) << std::endl; */
    /* std::cout << submatrix(A, 2, 1) << std::endl; */
    /* std::cout << submatrix(A, 2, 2) << std::endl; */
    ASSERT_TRUE( result == B );
}

TEST(matrices_feature, matrix_minor)
{
    Matrix A(3,3);
    A(0,0) = 3.;
    A(0,1) = 5.;
    A(0,2) = 0.;

    A(1,0) = 2.;
    A(1,1) = -1.;
    A(1,2) = -7.;

    A(2,0) = 6.;
    A(2,1) = -1.;
    A(2,2) = 5.;

    float result = mat_minor(A, 1, 0);

    ASSERT_TRUE( result == 25. );
}

TEST(matrices_feature, matrix_cofactor)
{
    Matrix A(3,3);
    A(0,0) = 3.;
    A(0,1) = 5.;
    A(0,2) = 0.;

    A(1,0) = 2.;
    A(1,1) = -1.;
    A(1,2) = -7.;

    A(2,0) = 6.;
    A(2,1) = -1.;
    A(2,2) = 5.;

    /* std::cout << mat_minor(A, 0, 0) << " " << cofactor(A, 0, 0) << std::endl; */
    /* std::cout << mat_minor(A, 0, 1) << " " << cofactor(A, 0, 1) << std::endl; */
    /* std::cout << mat_minor(A, 0, 2) << " " << cofactor(A, 0, 2) << std::endl; */
    
    /* std::cout << mat_minor(A, 1, 0) << " " << cofactor(A, 1, 0) << std::endl; */
    /* std::cout << mat_minor(A, 1, 1) << " " << cofactor(A, 1, 1) << std::endl; */
    /* std::cout << mat_minor(A, 1, 2) << " " << cofactor(A, 1, 2) << std::endl; */

    /* std::cout << mat_minor(A, 2, 0) << " " << cofactor(A, 2, 0) << std::endl; */
    /* std::cout << mat_minor(A, 2, 1) << " " << cofactor(A, 2, 1) << std::endl; */
    /* std::cout << mat_minor(A, 2, 2) << " " << cofactor(A, 2, 2) << std::endl; */

    ASSERT_TRUE( mat_minor(A, 0, 0) == -12. );
    ASSERT_TRUE( cofactor(A, 0, 0) == -12. );
    ASSERT_TRUE( mat_minor(A, 1, 0) == 25. );
    ASSERT_TRUE( cofactor(A, 1, 0) == -25. );
}

TEST(matrices_feature, det_3x3_matrix)
{
    Matrix A(3,3);
    A(0,0) = 1.;
    A(0,1) = 2.;
    A(0,2) = 6.;

    A(1,0) = -5.;
    A(1,1) = 8.;
    A(1,2) = -4.;

    A(2,0) = 2.;
    A(2,1) = 6.;
    A(2,2) = 4.;

    /* std::cout << cofactor(A, 0, 0) << std::endl; */
    /* std::cout << cofactor(A, 0, 1) << std::endl; */
    /* std::cout << cofactor(A, 0, 2) << std::endl; */
    /* std::cout << det(A) << std::endl; */
    ASSERT_TRUE(cofactor(A, 0, 0) == 56.);
    ASSERT_TRUE(cofactor(A, 0, 1) == 12.);
    ASSERT_TRUE(cofactor(A, 0, 2) == -46.);
    ASSERT_TRUE(det(A) == -196.);
}

TEST(matrices_feature, det_4x4_matrix)
{
    Matrix A(4,4);

    A(0,0) = -2.;
    A(0,1) = -8.;
    A(0,2) = 3.;
    A(0,3) = 5.;

    A(1,0) = -3.;
    A(1,1) = 1.;
    A(1,2) = 7.;
    A(1,3) = 3.;

    A(2,0) = 1.;
    A(2,1) = 2.;
    A(2,2) = -9.;
    A(2,3) = 6.;

    A(3,0) = -6.;
    A(3,1) = 7.;
    A(3,2) = 7.;
    A(3,3) = -9.;

    /* std::cout << A << std::endl; */
    ASSERT_TRUE(cofactor(A, 0, 0) == 690.);
    ASSERT_TRUE(cofactor(A, 0, 1) == 447.);
    ASSERT_TRUE(cofactor(A, 0, 2) == 210.);
    ASSERT_TRUE(cofactor(A, 0, 3) == 51.);
    ASSERT_TRUE(det(A) == -4071.);
    /* std::cout << B << std::endl; */
    /* std::cout << submatrix(A, 2, 1) << std::endl; */
    /* std::cout << submatrix(A, 0, 1) << std::endl; */
    /* std::cout << submatrix(A, 0, 2) << std::endl; */
    /* std::cout << submatrix(A, 1, 0) << std::endl; */
    /* std::cout << submatrix(A, 1, 1) << std::endl; */
    /* std::cout << submatrix(A, 1, 2) << std::endl; */
    /* std::cout << submatrix(A, 2, 0) << std::endl; */
    /* std::cout << submatrix(A, 2, 1) << std::endl; */
    /* std::cout << submatrix(A, 2, 2) << std::endl; */
    /* ASSERT_TRUE( result == B ); */
}

TEST(matrices_feature, 4x4_matrix_invertable)
{
    Matrix A(4,4);

    A(0,0) = 6.;
    A(0,1) = 4.;
    A(0,2) = 4.;
    A(0,3) = 4.;

    A(1,0) = 5.;
    A(1,1) = 5.;
    A(1,2) = 7.;
    A(1,3) = 6.;

    A(2,0) = 4.;
    A(2,1) = -9.;
    A(2,2) = 3.;
    A(2,3) = -7.;

    A(3,0) = 9.;
    A(3,1) = 1.;
    A(3,2) = 7.;
    A(3,3) = -6.;

    /* std::cout << A << std::endl; */
    ASSERT_TRUE(det(A) == -2120.);
}

TEST(matrices_feature, 4x4_matrix_not_invertable)
{
    Matrix A(4,4);

    A(0,0) = -4.;
    A(0,1) = 2.;
    A(0,2) = -2.;
    A(0,3) = -3.;

    A(1,0) = 9.;
    A(1,1) = 6.;
    A(1,2) = 2.;
    A(1,3) = 6.;

    A(2,0) = 0.;
    A(2,1) = -5.;
    A(2,2) = 1.;
    A(2,3) = -5.;

    A(3,0) = 0.;
    A(3,1) = 0.;
    A(3,2) = 0.;
    A(3,3) = 0.;

    /* std::cout << A << std::endl; */
    ASSERT_TRUE(det(A) == 0.);
}

TEST(matrices_feature, matrix_inverse)
{
    Matrix A(4,4);

    A(0,0) = -5.;
    A(0,1) = 2.;
    A(0,2) = 6.;
    A(0,3) = -8.;

    A(1,0) = 1.;
    A(1,1) = -5.;
    A(1,2) = 1.;
    A(1,3) = 8.;

    A(2,0) = 7.;
    A(2,1) = 7.;
    A(2,2) = -6.;
    A(2,3) = -7.;

    A(3,0) = 1.;
    A(3,1) = -3.;
    A(3,2) = 7.;
    A(3,3) = 4.;

    Matrix expected(4,4);
    expected[0][0] = 0.218045;
    expected[0][1] = 0.451128;
    expected[0][2] = 0.240602;
    expected[0][3] = -0.0451128;
    expected[1][0] = -0.808271;
    expected[1][1] = -1.45677;
    expected[1][2] = -0.443609;
    expected[1][3] = 0.520677;
    expected[2][0] = -0.0789474;
    expected[2][1] = -0.223684;
    expected[2][2] = -0.0526316;
    expected[2][3] = 0.197368;
    expected[3][0] = -0.522556;
    expected[3][1] = -0.81391;
    expected[3][2] = -0.300752;
    expected[3][3] = 0.306391;


    Matrix result = inverse(A);
    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */
    /* std::cout << det(A) << std::endl; */
    ASSERT_TRUE(det(A) == 532.);
    ASSERT_TRUE(cofactor(A,2,3) == -160.);
    ASSERT_NEAR(result[3][2], -160./532, 1.e-5);
    ASSERT_TRUE(cofactor(A,3,2) == 105.);
    ASSERT_NEAR(result[2][3], 105./532, 1.e-5);
    ASSERT_TRUE(result == expected);
}

TEST(matrices_feature, matrix_inverse_1)
{
    Matrix A(4,4);

    A(0,0) = 8.;
    A(0,1) = -5.;
    A(0,2) = 9.;
    A(0,3) = 2.;

    A(1,0) = 7.;
    A(1,1) = 5.;
    A(1,2) = 6.;
    A(1,3) = 1.;

    A(2,0) = -6.;
    A(2,1) = 0.;
    A(2,2) = 9.;
    A(2,3) = 6.;

    A(3,0) = -3.;
    A(3,1) = 0.;
    A(3,2) = -9.;
    A(3,3) = -4.;

    Matrix expected(4,4);

    expected[0][0] = -0.153846;
    expected[0][1] = -0.153846;
    expected[0][2] = -0.282051;
    expected[0][3] = -0.538462;
    expected[1][0] = -0.0769231;
    expected[1][1] = 0.123077;
    expected[1][2] = 0.025641;
    expected[1][3] = 0.0307692;
    expected[2][0] = 0.358974;
    expected[2][1] = 0.358974;
    expected[2][2] = 0.435897;
    expected[2][3] = 0.923077;
    expected[3][0] = -0.692308;
    expected[3][1] = -0.692308;
    expected[3][2] = -0.769231;
    expected[3][3] = -1.92308;

    Matrix result = inverse(A);
    ASSERT_TRUE(result == expected);
}

TEST(matrices_feature, matrix_inverse_2)
{
    Matrix A(4,4);

    A(0,0) = 9.;
    A(0,1) = 3.;
    A(0,2) = 0.;
    A(0,3) = 9.;

    A(1,0) = -5.;
    A(1,1) = -2.;
    A(1,2) = -6.;
    A(1,3) = -3.;

    A(2,0) = -4.;
    A(2,1) = 9.;
    A(2,2) = 6.;
    A(2,3) = 4.;

    A(3,0) = -7.;
    A(3,1) = 6.;
    A(3,2) = 6.;
    A(3,3) = 2.;

    Matrix expected(4,4);

    expected[0][0] = -0.0407407;
    expected[0][1] = -0.0777778;
    expected[0][2] = 0.144444;
    expected[0][3] = -0.222222;
    expected[1][0] = -0.0777778;
    expected[1][1] = 0.0333333;
    expected[1][2] = 0.366667;
    expected[1][3] = -0.333333;
    expected[2][0] = -0.0290123;
    expected[2][1] = -0.146296;
    expected[2][2] = -0.109259;
    expected[2][3] = 0.12963;
    expected[3][0] = 0.177778;
    expected[3][1] = 0.0666667;
    expected[3][2] = -0.266667;
    expected[3][3] = 0.333333;

    Matrix result = inverse(A);

    /* std::cout << A << std::endl; */
    /* std::cout << result << std::endl; */

    ASSERT_TRUE(result == expected);
}

TEST(matrices_feature, matrix_inverse_3)
{
    Matrix A(4,4);

    A(0,0) = 3.;
    A(0,1) = -9.;
    A(0,2) = 7.;
    A(0,3) = 3.;

    A(1,0) = 3.;
    A(1,1) = -8.;
    A(1,2) = 2.;
    A(1,3) = -9.;

    A(2,0) = -4.;
    A(2,1) = 4.;
    A(2,2) = 4.;
    A(2,3) = 1.;

    A(3,0) = -6.;
    A(3,1) = 5.;
    A(3,2) = -1.;
    A(3,3) = 1.;

    Matrix B(4,4);

    B[0][0] = 8.; 
    B[0][1] = 2.;
    B[0][2] = 2.;
    B[0][3] = 2.;

    B[1][0] = 3.;
    B[1][1] = -1.;
    B[1][2] = 7.;
    B[1][3] = 0.;

    B[2][0] = 7.;
    B[2][1] = 0.;
    B[2][2] = 5.;
    B[2][3] = 4.;

    B[3][0] = 6.;
    B[3][1] = -2.;
    B[3][2] = 0.;
    B[3][3] = 5.;

    /* std::cout << A << std::endl; */
    /* std::cout << B << std::endl; */
    /* std::cout << B * inverse(B) << std::endl; */
    /* std::cout << inverse(identity(4)) << std::endl; */

    ASSERT_TRUE(A * B * inverse(B) == A);
    ASSERT_TRUE(B * inverse(B) == identity(4));
}
