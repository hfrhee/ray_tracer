#pragma once
#include "color.h"
#include "object.h"

namespace ray_tracer {

  class Pattern {
    public:
      Pattern() : transform(identity(4)) {}

      void set_transform(const Matrix& t) { transform = t; }
      const Matrix& get_transform() const { return transform; }

      Color pattern_at_shape(const Object& object, const Tuple& point) const;
      virtual Color pattern_at(const Tuple& point) const = 0;

    private:
      Matrix transform;
  };

}
