# Ray Tracer Challenge

A 3D renderer build from scratch based on Jamis Buck's book "The Ray Tracer Challenge".

http://raytracerchallenge.com/

Following the Test Driven Development approach each chapter presents a set of tests that drive the development of a 3D renderer beginning with the simple and proceeding to the more complex. 

**Current status:** In progress. [working on Chapter 11]

Here are some images rendered so far:

![img05](result/img05.png)

![img06](result/img06.png)

![img07](result/img07.png)

The project is implemented in C++ and uses the unit test framework Google Test.

## How to build:

`mkdir build`

`cmake ../`

`cmake --build .`
