#pragma once
#include <vector>
#include "tuple.h"
#include "ray.h"
#include "object.h"

namespace ray_tracer{

  class Sphere : public Object {
    public:

      // TODO change to unique_ptr
      static std::shared_ptr<Sphere> create();
      static std::shared_ptr<Sphere> create_glass_sphere(); 

      bool operator==(const Sphere& other) const;


      virtual std::vector<Intersection> local_intersect(const Ray& r_in) const override;
      virtual Tuple local_normal_at(const Tuple& point_in) const override;

    private:
      // private ctr to enforce using the factory method for creating instance.
      Sphere() : Object("Sphere") {}

      virtual bool doCompare(const Object& other) const override;

      // Sphere at origin with radius 1.
      Tuple center {0., 0., 0., 1};
      float radius {1.};
  };
}
