#pragma once
#include <iostream>
#include <array>
#include "common.h"

namespace ray_tracer {

  using std::size_t;

  class Tuple {
    public:
      enum {x, y, z, w};
      static const size_t N = 4;

      std::array<double, N> data;

      Tuple() {
        data = {0., 0., 0., 0.};
      }

      Tuple(double x, double y, double z, double w) {
        data = {x,y,z,w};
      }

      Tuple(const Tuple& other) : data(other.data) {}

      double& operator[](int idx) { return data[idx]; }
      const double& operator[](int idx) const { return data[idx]; }
      
      Tuple operator-() const;
      Tuple operator*(double scalar) const;
      Tuple operator/(double scalar) const;

      bool is_point() const {
        /* return w == 1.0; */
        return data[3] == 1.;
      }

      bool is_vector() const {
        /* return w == 0.0; */
        return data[3] == 0.;
      }

      Tuple normalize() const;

      Tuple reflect(const Tuple& normal) const; 
  };

  std::ostream &operator<<(std::ostream &os, const Tuple& tuple);

  bool operator==(const Tuple& lhs, const Tuple& rhs); 
  Tuple operator+(const Tuple& lhs, const Tuple& rhs);
  Tuple operator-(const Tuple& lhs, const Tuple& rhs);

  Tuple point(double x, double y, double z);
  Tuple vector(double x, double y, double z);

  double magnitude(const Tuple& tuple);
  /* Tuple normalize(const Tuple& tuple); */

  double dot(const Tuple& a, const Tuple& b);
  Tuple cross(const Tuple& a, const Tuple& b);
}
