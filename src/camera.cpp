#include "camera.h"

namespace ray_tracer {

  Camera::Camera(int hsize, int vsize, double field_of_view)
    : hsize(hsize), vsize(vsize), field_of_view(field_of_view), transform(identity(4)) {
      const double half_view = std::tan(field_of_view/2);
      const double aspect = static_cast<double>(hsize)/static_cast<double>(vsize);

      if (aspect >= 1) {
        half_width = half_view;
        half_height = half_view / aspect;
      } else {
        half_width = half_view * aspect;
        half_height = half_view;
      }
    
      pixel_size = (half_width*2) / hsize;
    }

  Ray Camera::ray_for_pixel(int px, int py) const {

    // the offset from the edge of teh canvas to the pixel's center
    double xoffset = (static_cast<double>(px) + .5) * pixel_size;
    double yoffset = (static_cast<double>(py) + .5) * pixel_size;
  
    // the untransformed coordinates of the pixel in world space.
    // (remember that the camera looks toward -z, so +x is the the left)
    double world_x = half_width - xoffset;
    double world_y = half_height - yoffset;

    // using the camera matrix, transform the canvas point and the origin,
    // and ten compute the ray's direction vector.
    // (remember that the canvas is at z=-1)
    Tuple pixel = inverse(transform) * point(world_x, world_y, -1);
    Tuple origin = inverse(transform) * point(0., 0., 0.);
    Tuple direction = (pixel - origin).normalize();

    return Ray(origin, direction);
  }

  Canvas Camera::render(const World& world) const {
    Canvas result(hsize, vsize);

    for (int y=0; y<vsize; ++y) {
      for (int x=0; x<hsize; ++x) {
        Ray ray = ray_for_pixel(x,y);
        Color color = world.color_at(ray);
        result.write_pixel(x,y,color);
      }
    }
    return result;
  }
}
