#include "tuple.h"

namespace ray_tracer {

  Tuple point(double x, double y, double z) {
    return Tuple(x, y, z, 1.0); 
  }

  Tuple vector(double x, double y, double z) {
    return Tuple(x, y, z, 0.0); 
  }

  bool operator==(const Tuple& lhs, const Tuple& rhs) {
    bool result = true;

    for(size_t i=0; i<Tuple::N; ++i) {
      result = result && ALMOST_EQUAL(lhs[i], rhs[i]);
    }

    return result;
  } 

  Tuple operator+(const Tuple& lhs, const Tuple& rhs) {
    Tuple result;
    
    for (size_t i=0; i<Tuple::N; ++i) {
      result[i] = lhs[i] + rhs[i];
    }

    return result;
  }

  Tuple operator-(const Tuple& lhs, const Tuple& rhs) {
    Tuple result;
    
    for (size_t i=0; i<Tuple::N; ++i) {
      result[i] = lhs[i] - rhs[i];
    }
    return result;
  }

  Tuple Tuple::operator-() const {
    return Tuple(0., 0., 0., 0.) - *this;   
  }

  Tuple Tuple::operator*(double scalar) const {
    Tuple result;
    
    for (size_t i=0; i<Tuple::N; ++i) {
      result[i] = (*this)[i] * scalar;
    }

    return result;
  }

  Tuple Tuple::operator/(double scalar) const {
    Tuple result;
    
    for (size_t i=0; i<Tuple::N; ++i) {
      result[i] = (*this)[i] / scalar;
    }

    return result;
  }

  double magnitude(const Tuple& tuple) {
    double result;

    result = tuple[0]*tuple[0] + tuple[1]*tuple[1] + tuple[2]*tuple[2];
    result = std::sqrt(result);

    return result;
  }
  
  Tuple Tuple::normalize() const {
    Tuple result;
    double abs = magnitude(*this);

    for(size_t i=0; i<Tuple::N; ++i) {
      result[i] = (*this)[i] /abs;
    }

    return result;
  }

  Tuple Tuple::reflect(const Tuple& normal) const {
    /* Tuple result = vector(1., 1., 0.); */
    /* std::cout << normal << std::endl; */

    return (*this) - normal * 2 * dot((*this), normal);
  }

  /* Tuple normalize(const Tuple& tuple) { */
  /*   Tuple result; */
  /*   double abs = magnitude(tuple); */

  /*   for(size_t i=0; i<Tuple::N; ++i) { */
  /*     result[i] = tuple[i] /abs; */
  /*   } */

  /*   return result; */
  /* } */

  double dot(const Tuple& a, const Tuple& b) {
    double result = 0.;

    for(size_t i=0; i<Tuple::N; ++i) {
      result += a[i] * b[i];
    }
  
    return result;
  }

  Tuple cross(const Tuple& a, const Tuple& b) {
    Tuple result;

    result[0] = a[1] * b[2] - a[2] * b[1];
    result[1] = a[2] * b[0] - a[0] * b[2];
    result[2] = a[0] * b[1] - a[1] * b[0];
    result[3] = 0.;

    return result;
  }

  std::ostream &operator<<(std::ostream &os, const Tuple& tuple) {
    if(tuple.is_point())
      os << "point";
    else if(tuple.is_vector())
      os << "vector";
    else
      os << "tuple";

    os << "(" << tuple[Tuple::x] << ", " << tuple[Tuple::y] << ", " << tuple[Tuple::z];
    if (!(tuple.is_point()) && !(tuple.is_vector()))
      os << ", " << tuple[Tuple::w];
    os << ")";

    return os;
  }
}

