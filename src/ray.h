#pragma once
#include "tuple.h"
#include "matrix.h"

namespace ray_tracer {
  class Ray {
    public:
      Tuple origin;
      Tuple direction;

      explicit Ray() : origin(point(0.,0.,0.)), direction(vector(0., 0., 0.)) {}

      Ray(const Tuple& origin, const Tuple& direction)
        : origin(origin), direction(direction) {
          assert(this->origin.is_point()); 
          assert(this->direction.is_vector()); 
        }
  
      Tuple position(double t) const;
      Ray transform(const Matrix& matrix) const;
  };

}
