#pragma once

#include <ostream>
#include <array>
#include <vector>
#include <memory>
#include "color.h"

namespace ray_tracer {

  class Canvas{
    public:
      int width;
      int height;
      std::vector< std::vector<Color> > pixel;

      Canvas(int width ,int height);

      Color pixel_at(int x, int y) const;

      void write_pixel(int x, int y, Color color);
      
      void to_ppm(std::ostream &os) const;
  
  };

  
}
