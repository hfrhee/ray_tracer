#pragma once
#include "common.h"
#include "matrix.h"
#include "ray.h"


namespace ray_tracer {

  using std::size_t;

  Matrix translation(float x, float y, float z);

  Matrix scaling(float x, float y, float z); 

  // r in radians
  Matrix rotation_x(float r); 

  Matrix rotation_y(float r);

  Matrix rotation_z(float r);

  Matrix shearing(float x_y, float x_z, float y_x, float y_z, float z_x, float z_y); 

  /* Ray transform(const Ray& ray, const Matrix& matrix); */ 
  Matrix view_transform(const Tuple& from, const Tuple& to, const Tuple& up);

}
