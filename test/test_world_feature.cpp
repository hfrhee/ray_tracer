#include <iostream>
#include "gtest/gtest.h"
#include "world.h"
#include "light.h"
#include "tuple.h"
#include "color.h"
#include "sphere.h"
#include "plane.h"


using namespace ray_tracer;

TEST(world_feature, create_world) {
  World w;

  ASSERT_TRUE(w.get_objects().empty());
  ASSERT_TRUE(w.get_lights().empty());
}

TEST(world_feature, default_world) {
  Light light(point(-10., 10, -10.), Color(1., 1., 1.));
  const auto s1 = Sphere::create(); 
  Material m;
  m.set_color(Color(0.8, 1., .6));
  m.set_diffuse(.7);
  m.set_specular(.2);
  s1->set_material(m);

  const auto s2 = Sphere::create(); 
  s2->set_transform(scaling(.5, .5, .5));

  World w = World::default_world();

  ASSERT_TRUE(w.get_lights()[0] == light);
  ASSERT_TRUE(w.contains(*s1));
  ASSERT_TRUE(w.contains(*s2));
}

TEST(world_feature, intersect_world_with_ray) {
  World w = World::default_world();

  Ray r(point(0., 0., -5), vector(0., 0., 1.));

  std::vector<Intersection> xs = w.intersect(r);

  ASSERT_TRUE(xs.size() == 4);
  ASSERT_TRUE(xs[0].get_t() == 4.);
  ASSERT_TRUE(xs[1].get_t() == 4.5);
  ASSERT_TRUE(xs[2].get_t() == 5.5);
  ASSERT_TRUE(xs[3].get_t() == 6.);
}

TEST(world_feature, shading_intersection) {
  World w = World::default_world();

  Ray r(point(0., 0., -5), vector(0., 0., 1.));

  const auto shape = w.get_objects()[0];

  Intersection i(4., shape);

  Comps comps = i.prepare_computations(r);

  Color c = w.shade_hit(comps);

  ASSERT_TRUE( c == Color(0.38066, 0.47583, 0.2855) );
}

TEST(world_feature, shading_intersection_from_inside) {
  World w = World::default_world();

  w.clear_lights();
  w.set_light(Light(point(0., .25, 0.), Color(1., 1., 1.)));

  Ray r(point(0., 0., 0), vector(0., 0., 1.));

  const auto shape = w.get_objects()[1];

  Intersection i(.5, shape);

  Comps comps = i.prepare_computations(r);

  Color c = w.shade_hit(comps);

  ASSERT_TRUE( c == Color(0.90498, 0.90498, 0.90498) );
}

TEST(world_feature, color_when_ray_misses) {
  World w = World::default_world();
  Ray r(point(0., 0., -5), vector(0., 1., 0.));
  Color result = w.color_at(r);

  ASSERT_TRUE(result == Color(0., 0., 0.));
}

TEST(world_feature, color_when_ray_hits) {
  World w = World::default_world();
  Ray r(point(0., 0., -5), vector(0., 0., 1.));
  Color result = w.color_at(r);

  ASSERT_TRUE(result == Color(.38066, 0.47583, 0.2855));
}

TEST(world_feature, color_with_intersection_behind_ray) {
  World w = World::default_world();

  const auto outer = w.get_objects()[0];
  outer->get_material().set_ambient(1.);
  /* std::cout << outer->get_material().get_ambient() << std::endl; */

  const auto inner = w.get_objects()[1];
  inner->get_material().set_ambient(1.);
  /* std::cout << inner->get_material().get_ambient() << std::endl; */
   
  Ray r(point(0., 0., .75), vector(0., 0., -1.));
  Color result = w.color_at(r);
  Color expected = inner->get_material().get_color();
  /* std::cout << result << ", " << expected << std::endl; */

  ASSERT_TRUE(result == expected);
}

TEST(world_feature, no_shadow) {
  World w = World::default_world();
  Tuple p = point(0., 10., 0.);

  ASSERT_TRUE(w.is_shadowed(p) == false);
}

TEST(world_feature, shadow_when_object_between_point_and_light) {
  World w = World::default_world();
  Tuple p = point(10., -10., 10.);

  ASSERT_TRUE(w.is_shadowed(p) == true);
}

TEST(world_feature, no_shadow_when_object_behind_point) {
  World w = World::default_world();
  Tuple p = point(-2., 2., -2.);

  ASSERT_TRUE(w.is_shadowed(p) == false);
}

TEST(world_feature, shade_hit_is_given_intersection_in_shadow) {
  Light point_light(point(0., 0, -10.), Color(1., 1., 1.));

  const auto s1 = Sphere::create(); 

  const auto s2 = Sphere::create(); 
  s2->set_transform(translation(0., 0., 10.));


  World::vector_obj world_obj {s1, s2};
  World::vector_light world_light {point_light};

  World world(world_obj, world_light);

  Ray r(point(0., 0., 5.), vector(0., 0., 1.));
  Intersection i(4., s2);

  Comps comps = i.prepare_computations(r);

  Color result = world.shade_hit(comps);

  ASSERT_TRUE(result == Color(.1, .1, .1));
}

TEST(world_feature, reflected_color_for_nonreflective_material) {
  World w = World::default_world();
  Ray r(point(0., 0., 0.), vector(0., 0., 1.));

  auto shape = w.get_objects()[1];

  shape->get_material().set_ambient(1.);

  Intersection i(1., shape);

  Comps comps = i.prepare_computations(r);
  Color color = w.reflected_color(comps);

  ASSERT_TRUE(color == Color(0., 0., 0.));
}

TEST(world_feature, reflected_color_for_reflective_material) {
  World w = World::default_world();

  auto shape = Plane::create();

  shape->get_material().set_reflective(.5);
  shape->set_transform(translation(0., -1., 0.));

  // add shape to world
  w.add_object(shape);

  /* auto obj_list = w.get_objects(); */
  /* std::copy(obj_list.begin(), */
  /*     obj_list.end(), */
  /*     std::ostream_iterator<std::shared_ptr<Object> >(std::cout," ")); */
  
  Ray r(point(0., 0., -3.), vector(0., -std::sqrt(2.)/2., std::sqrt(2.)/2.));

  Intersection i(std::sqrt(2.), shape);

  Comps comps = i.prepare_computations(r);

  Color color = w.reflected_color(comps);


  ASSERT_TRUE(color == Color(0.19032, 0.2379, 0.14274));
}

TEST(world_feature, shade_hit_with_reflective_material) {
  World w = World::default_world();

  auto shape = Plane::create();
  shape->get_material().set_reflective(.5);
  shape->set_transform(translation(0., -1., 0.));

  w.add_object(shape);

  Ray r(point(0., 0., -3.), vector(0., -std::sqrt(2.)/2., std::sqrt(2.)/2.));

  Intersection i(std::sqrt(2.), shape);

  Comps comps = i.prepare_computations(r);

  Color color = w.shade_hit(comps);

  ASSERT_TRUE(color == Color(0.87677, 0.92436, 0.82918));
}

TEST(world_feature, color_at_with_mutually_reflective_surfaces) {

  auto lower = Plane::create();
  lower->get_material().set_reflective(1.);
  lower->set_transform(translation(0., -1., 0.));


  auto upper = Plane::create();
  upper->get_material().set_reflective(1.);
  upper->set_transform(translation(0., 1., 0.));

  std::vector<std::shared_ptr<Object>> objects{lower, upper};
  std::vector<Light> lights{Light(point(0., 0., 0.), Color(1.,1.,1.))};

  World w(objects, lights);

  Ray r(point(0., 0., 0.), vector(0., 1., 0.));

  Color result = w.color_at(r);

  ASSERT_TRUE(result == Color(11.4, 11.4, 11.4));
}

TEST(world_feature, reflected_color_at_maximum_recursive_depth) {
  World w = World::default_world();

  auto shape = Plane::create();
  shape->get_material().set_reflective(.5);
  shape->set_transform(translation(0., -1., 0.));

  w.add_object(shape);

  Ray r(point(0., 0., -3.), vector(0., -std::sqrt(2.)/2., std::sqrt(2.)/2.));

  Intersection i(std::sqrt(2.), shape);

  Comps comps = i.prepare_computations(r);

  Color color = w.reflected_color(comps, 0);

  ASSERT_TRUE(color == Color(0., 0., 0.));
}




