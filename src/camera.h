#pragma once
#include "common.h"
#include "transform.h"
#include "ray.h"
#include "world.h"
#include "canvas.h"

namespace ray_tracer {

  class Camera {
    public:
      Camera(int hsize, int vsize, double field_of_view);
      Camera (const Camera&) = default;
      Camera (Camera&&) = default;
      Camera& operator=(const Camera&) = default;

      int get_hsize() const { return hsize; }
      int get_vsize() const { return vsize; }
      double get_field_of_view() const { return field_of_view; }
      Matrix get_transform() const { return transform; }
      void set_transform(const Matrix& other) { transform = other; }
      double get_pixel_size() const { return pixel_size; };

      Ray ray_for_pixel(int px, int py) const;

      Canvas render(const World& world) const;

    private:
      int hsize;
      int vsize;
      double field_of_view;
      Matrix transform;
      double pixel_size;
      double half_width;
      double half_height;
  };
}
