#include <algorithm>
#include <typeinfo>
#include "world.h"
#include "sphere.h"

namespace ray_tracer {


  std::vector<std::shared_ptr<Object>> World::get_objects() const {
    return objects;
  }

  std::vector<Light> World::get_lights() const {
    return lights;
  }
  
  void World::set_light(const Light& light) {
    lights.emplace_back(light);
    // add light at front of vector
    /* std::rotate(lights.begin(), lights.begin()+1, lights.end()); */
  }

  void World::clear_lights() {
    lights.clear();
  }
  
  bool World::contains(const Object& obj) const {
    for (const auto elem : objects) {
      if (*elem == obj) {
        return true;
      }
    }
    return false;
  }
 
  World World::default_world() {

    const auto s1 = Sphere::create(); 
    Material m;
    m.set_color(Color(0.8, 1., .6));
    m.set_diffuse(.7);
    m.set_specular(.2);
    s1->set_material(m);

    const auto s2 = Sphere::create(); 
    s2->set_transform(scaling(.5, .5, .5));

    Light light(point(-10., 10, -10.), Color(1., 1., 1.));

    vector_obj res_objects {s1, s2};
    vector_light res_lights {light};

    World result(res_objects, res_lights);

    return result;
  }
  
  std::vector<Intersection> World::intersect(const Ray& ray) const {
    std::vector<Intersection> result;

    for (const auto obj : objects) {
      std::vector<Intersection> tmp = obj->intersect(ray);
      result.insert(std::end(result), std::begin(tmp), std::end(tmp));
    }

    std::sort(std::begin(result), std::end(result),
        [](const auto lhs, const auto rhs) { return lhs.get_t() < rhs.get_t();} );

    return result;
  }

  // Return color at interesection encapsulated by comps in the given world.
  Color World::shade_hit(const Comps& comps, const int remaining) const {
    // TODO: iterate over elements in lights
    Color result;

    bool shadowed = is_shadowed(comps.over_point);

    Color surface = comps.object->get_material().lighting(
        *comps.object,
        this->get_lights()[0],
        comps.over_point,
        comps.eyev,
        comps.normalv,
        shadowed 
        );

    Color reflected = this->reflected_color(comps, remaining);

    result = surface + reflected;

    return result;
  }

  Color World::color_at(const Ray& ray, const int remaining) const {
    Color result;

    const auto xs = this->intersect(ray);
    const auto i = hit(xs);

    if (i.has_value()) {
      Comps comps = i->prepare_computations(ray);
      result = this->shade_hit(comps, remaining);
    }

    return result;
  }

  bool World::is_shadowed(const Tuple& point) const {

    Tuple v = lights[0].get_position() - point; 

    double distance = magnitude(v);
    Tuple direction = v.normalize();
    Ray r(point, direction);

    std::vector<Intersection> xs = this->intersect(r);

    const auto h = hit(xs);

    return h.has_value() && h.value().get_t() < distance;
  }

  Color World::reflected_color(const Comps& comps, const int remaining) const {
    Color result;

    if (remaining <= 0) {
      return Color(0., 0., 0.);
    }

    if (comps.object->get_material().get_reflective() == 0) {
      return Color(0., 0., 0.); 
    }
    
    Ray reflect_ray(comps.over_point, comps.reflectv);

    Color color = this->color_at(reflect_ray, remaining - 1);

    result = color * comps.object->get_material().get_reflective();
    
    return result;
  
  }
     
  void World::add_object(std::shared_ptr<Object> obj) {
    /* objects.emplace_back(std::make_shared<Object>(obj)); */ 
    /* auto tmp = std::make_shared<Object>(obj); */
    /* objects.emplace_back(tmp); */ 
    objects.emplace_back(obj);
  }

}
