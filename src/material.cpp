#include "material.h"
#include "common.h"
#include "pattern.h"

namespace ray_tracer {

  bool Material::operator==(const Material& other) const {
    bool result = true;

    result = result && color == other.get_color();
    result = result && ALMOST_EQUAL(ambient, other.get_ambient());
    result = result && ALMOST_EQUAL(diffuse, other.get_diffuse());
    result = result && ALMOST_EQUAL(specular, other.get_specular());
    result = result && ALMOST_EQUAL(shininess, other.get_shininess());
     
    return result;
  }

  Color Material::lighting(const Object& object, const Light& light, const Tuple& point, const Tuple& eyev, const Tuple& normalv, bool in_shadow) const {
    assert(point.is_point());

    Color result;
    Color point_color;

    if (pattern != nullptr) {
      point_color = pattern->pattern_at_shape(object, point);
    } else {
      point_color = color;
    }

    // Combine surface color with the light's color/intensity
    Color effective_color = hadamard_product(point_color, light.get_intensity());

    // find direction of light source
    Tuple lightv = (light.get_position() - point).normalize();

    // compute the ambient contribution
    Color res_ambient = effective_color * this->ambient;

    double light_dot_normal = dot(lightv, normalv);
     
    Color res_diffuse;
    Color res_specular;

    if(!in_shadow) {
      if (light_dot_normal < 0.) {
        res_diffuse = Color(0., 0., 0.); 
        res_specular = Color(0., 0., 0.);
      } else {
        res_diffuse = effective_color * this->diffuse * light_dot_normal; 
        
        Tuple reflectv = (-lightv).reflect(normalv);
        double reflect_dot_eye = dot(reflectv, eyev);

        if (reflect_dot_eye <= 0.) {
          res_specular = Color(0., 0., 0.);
        } else {
          double factor = std::pow(reflect_dot_eye, this->shininess);
          res_specular = light.get_intensity() * this->specular * factor;
        }
      }
    }
    
    result = res_ambient + res_diffuse + res_specular;

    return result;
  }

}
