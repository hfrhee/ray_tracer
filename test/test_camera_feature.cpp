#include <iostream>
#include "gtest/gtest.h"
#include "camera.h"


using namespace ray_tracer;

TEST(camera_feature, constructing_camera) {
  int hsize = 160;
  int vsize = 120;
  double field_of_view = pi()/2;

  Camera c(hsize, vsize, field_of_view);

  ASSERT_TRUE(c.get_hsize() == 160);
  ASSERT_TRUE(c.get_vsize() == 120);
  ASSERT_TRUE(c.get_field_of_view() == pi()/2);
  ASSERT_TRUE(c.get_transform() == identity(4));
}

TEST(camera_feature, pixel_size_for_horizontal_canvas) {
  Camera c(200, 125, pi()/2);

  ASSERT_NEAR(c.get_pixel_size(), 0.01, 1.0e-5);
}

TEST(camera_feature, pixel_size_for_vertical_canvas) {
  Camera c(125, 200, pi()/2);

  ASSERT_NEAR(c.get_pixel_size(), 0.01, 1.0e-5);
}

TEST(camera_feature, constructing_ray_through_center_of_canvas) {
  Camera c(201, 101, pi()/2);
  Ray r = c.ray_for_pixel(100, 50);

  ASSERT_TRUE(r.origin == point(0., 0., 0.));
  ASSERT_TRUE(r.direction == vector(0., 0., -1.));
}

TEST(camera_feature, constructing_ray_through_corner_of_canvas) {
  Camera c(201, 101, pi()/2);
  Ray r = c.ray_for_pixel(0, 0);

  ASSERT_TRUE(r.origin == point(0., 0., 0.));
  ASSERT_TRUE(r.direction == vector(0.66519, 0.33259, -0.66851));
}

TEST(camera_feature, constructing_ray_when_camera_is_transformed) {
  Camera c(201, 101, pi()/2);
  c.set_transform(rotation_y(pi()/4) * translation(0., -2., 5.));
  Ray r = c.ray_for_pixel(100, 50);

  ASSERT_TRUE(r.origin == point(0., 2., -5.));
  ASSERT_TRUE(r.direction == vector(std::sqrt(2)/2, 0., -std::sqrt(2)/2));
}

TEST(camera_feature, rendering_world_with_camera) {
  World w = World::default_world();
  Camera c(11, 11, pi()/2);
  Tuple from = point(0., 0., -5.);
  Tuple to = point(0., 0., 0.);
  Tuple up = vector(0., 1., 0.);

  c.set_transform(view_transform(from, to, up));
  Canvas image = c.render(w);

  Color result = image.pixel_at(5., 5.);

  ASSERT_TRUE(result == Color(0.38066, 0.47583, 0.2855));
}
