#include "gtest/gtest.h"
#include "light.h"

using namespace ray_tracer;

TEST(ligths_feature, position_and_intensity)
{
  Tuple position = point(0., 0., 0.);
  Color intensity = Color(1., 1., 1.);
  Light result(position, intensity);

  ASSERT_TRUE(result.get_position() == position);
  ASSERT_TRUE(result.get_intensity() == intensity);

  

}
