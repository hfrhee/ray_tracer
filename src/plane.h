#pragma once
#include <vector>
#include "tuple.h"
#include "ray.h"
#include "object.h"

namespace ray_tracer{

  class Plane : public Object {
    public:

      // create plane in xz with normal in positive y direction
      static std::shared_ptr<Plane> create() {
        
        return std::make_shared<Plane>( Plane() );
      }

      bool operator==(const Plane& other) const;


      virtual std::vector<Intersection> local_intersect(const Ray& r_in) const override;
      virtual Tuple local_normal_at(const Tuple& point_in) const override;

    private:
      // private ctr to enforce using the factory method for creating instance.
      Plane() : Object("Plane") {}

      virtual bool doCompare(const Object& other) const override;

  };
}
