#include "plane.h"

namespace ray_tracer{

  bool Plane::operator==(const Plane& other) const {
    bool result;

    result = get_name() == other.get_name();
    result = result && (get_transform() == other.get_transform());
    result = result && (get_material() == other.get_material());

    return result;
  }

  bool Plane::doCompare(const Object& other) const {
    const auto *s = dynamic_cast<const Plane*>(&other); 

    return (s != nullptr && *this == *s);
  } 

  std::vector<Intersection> Plane::local_intersect(const Ray& r) const {
    std::vector<Intersection> result; 

    if (std::abs(r.direction[1]) < EPSILON) {
      return result;
    }

    double t = -r.origin[1] / r.direction[1];
    result.emplace_back(Intersection(t, shared_from_this()));

    return result;
  }
  
  Tuple Plane::local_normal_at(const Tuple& point_in) const {
    assert(point_in.is_point());
    return vector(0., 1., 0.);
  } 
}
