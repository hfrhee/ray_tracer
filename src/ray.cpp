#include "ray.h"

namespace ray_tracer {
  Tuple Ray::position(double t) const {
    Tuple result; 

    result = this->origin + this->direction * t;

    assert(result.is_point());

    return result;
  }

  Ray Ray::transform(const Matrix& matrix) const {
    Ray result(point(0., 0., 0.), vector(0., 0., 0.));
    result.origin = matrix * origin;
    result.direction = matrix * direction;

    return result;
  }
 
}
