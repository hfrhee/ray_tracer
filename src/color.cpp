#include "color.h"

namespace ray_tracer {
  Color hadamard_product(const Color& c1, const Color& c2) {
    Color result;

    /* result.red = c1.red * c2.red; */
    /* result.green = c1.green * c2.green; */
    /* result.blue = c1.blue * c2.blue; */

    /* result.x = result.red; */
    /* result.y = result.green; */
    /* result.z = result.blue; */
    for(size_t i=0; i<Tuple::N; ++i) {
      result[i] = c1[i] * c2[i];
    }

    return result;
  }
}
