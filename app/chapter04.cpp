#include <iostream>
#include <fstream>
#include "common.h"
#include "canvas.h"
#include "transform.h"

using namespace ray_tracer;

int main() {

  int width = 800;
  Canvas canvas(width, width);

  Color red = Color(1., 0., 0.);

  float radius = width * 3/8; 

  // twelve o'clock
  Tuple hour = point(0., 0., 1.);
  Matrix rotate = rotation_y(pi()/6);
  Matrix scale = scaling(radius, 0., radius);
  Matrix translate = translation(width/2, 0., width/2);


  for (int n=0; n<12; ++n) {
    hour = rotate * hour;

    Tuple p = translate * scale * hour; 

    for (int i=-3; i<3; ++i) {
      for (int j=-3; j<3; ++j) {
        canvas.write_pixel((int)p[0] + i, (int)p[2] + j, red);
      }
    }
  }

  std::ofstream img("img04.ppm");
  canvas.to_ppm(img);
  img.close();

  return 0;
}
