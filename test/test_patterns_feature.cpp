#include <iostream>
#include "gtest/gtest.h"
#include "color.h"
#include "pattern.h"
#include "stripe_pattern.h"
#include "gradient_pattern.h"
#include "ring_pattern.h"
#include "checkers_pattern.h"
#include "sphere.h"

using namespace ray_tracer;

TEST(patterns_feature, creating_stripe_pattern) {
  StripePattern p(color::white, color::black);

  ASSERT_TRUE(p.get_a() == color::white);
  ASSERT_TRUE(p.get_b() == color::black);
}

TEST(patterns_feature, stripe_pattern_is_constant_in_y) {
  StripePattern p(color::white, color::black);

  ASSERT_TRUE(p.pattern_at(point(0., 0., 0.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(0., 1., 0.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(0., 2., 0.)) == color::white);
}

TEST(patterns_feature, stripe_pattern_is_constant_in_z) {
  StripePattern p(color::white, color::black);

  ASSERT_TRUE(p.pattern_at(point(0., 0., 0.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(0., 0., 1.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(0., 0., 2.)) == color::white);
}

TEST(patterns_feature, stripe_pattern_alternates_in_x) {
  StripePattern p(color::white, color::black);

  ASSERT_TRUE(p.pattern_at(point(0., 0., 0.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(.9, 0., 0.)) == color::white);
  ASSERT_TRUE(p.pattern_at(point(1., 0., 0.)) == color::black);
  ASSERT_TRUE(p.pattern_at(point(-.1, 0., 0.)) == color::black);
  ASSERT_TRUE(p.pattern_at(point(-1., 0., 0.)) == color::black);
  ASSERT_TRUE(p.pattern_at(point(-1.1, 0., 0.)) == color::white);
}

TEST(patterns_feature, stripes_with_object_transformation) {
  const auto object = Sphere::create();
  object->set_transform(scaling(2., 2., 2.));

  StripePattern p(color::white, color::black);

  Color c = p.pattern_at_shape(*object, point(1.5, 0., 0.));

  ASSERT_TRUE(c == color::white);
}

TEST(patterns_feature, stripes_with_pattern_transformation) {
  const auto object = Sphere::create();

  StripePattern p(color::white, color::black);
  p.set_transform(scaling(2., 2., 2.));

  Color c = p.pattern_at_shape(*object, point(1.5, 0., 0.));

  ASSERT_TRUE(c == color::white);
}

TEST(patterns_feature, stripes_with_both_object_and_pattern_transformation) {
  const auto object = Sphere::create();
  object->set_transform(scaling(2., 2., 2.));

  StripePattern p(color::white, color::black);
  p.set_transform(translation(.5, 0., 0.));

  Color c = p.pattern_at_shape(*object, point(2.5, 0., 0.));

  ASSERT_TRUE(c == color::white);
}

class Test_Pattern : public Pattern {
  public:
    virtual Color pattern_at(const Tuple& point) const {
      return Color(point[0], point[1], point[2]);
    }
};

TEST(patterns_feature, default_pattern_transformation) {
  Test_Pattern pattern;

  ASSERT_TRUE(pattern.get_transform() == identity(4));
}

TEST(patterns_feature, assigning_transformation) {
  Test_Pattern pattern;
  pattern.set_transform(translation(1., 2., 3.));

  ASSERT_TRUE(pattern.get_transform() == translation(1., 2., 3.));
}

TEST(patterns_feature, pattern_with_object_transformation) {
  const auto s = Sphere::create();
  s->set_transform(scaling(2., 2., 2.));

  Test_Pattern pattern;

  Color c = pattern.pattern_at_shape(*s, point(2., 3., 4.));
  ASSERT_TRUE(c == Color(1., 1.5, 2.));
}

TEST(patterns_feature, pattern_with_pattern_transformation) {
  const auto s = Sphere::create();

  Test_Pattern pattern;
  pattern.set_transform(scaling(2., 2., 2.));

  Color c = pattern.pattern_at_shape(*s, point(2., 3., 4.));
  ASSERT_TRUE(c == Color(1., 1.5, 2.));
}

TEST(patterns_feature, gradient_linearly_interpolates_between_colors) {

  GradientPattern pattern(color::white, color::black);

  Color c1 = pattern.pattern_at(point(0., 0., 0.));
  Color c2 = pattern.pattern_at(point(0.25, 0., 0.));
  Color c3 = pattern.pattern_at(point(0.5, 0., 0.));
  Color c4 = pattern.pattern_at(point(0.75, 0., 0.));

  ASSERT_TRUE(c1 == color::white);
  ASSERT_TRUE(c2 == Color(.75, .75, .75));
  ASSERT_TRUE(c3 == Color(.5, .5, .5));
  ASSERT_TRUE(c4 == Color(.25, .25, .25));
}

TEST(patterns_feature, ring_should_extend_in_both_x_and_z) {

  RingPattern pattern(color::white, color::black);

  Color c1 = pattern.pattern_at(point(0., 0., 0.));
  Color c2 = pattern.pattern_at(point(1., 0., 0.));
  Color c3 = pattern.pattern_at(point(0., 0., 1.));
  Color c4 = pattern.pattern_at(point(0.708, 0., 0.708));

  ASSERT_TRUE(c1 == color::white);
  ASSERT_TRUE(c2 == color::black);
  ASSERT_TRUE(c3 == color::black);
  ASSERT_TRUE(c4 == color::black);
}

TEST(patterns_feature, checkers_should_repeat_in_x) {

  CheckersPattern pattern(color::white, color::black);

  Color c1 = pattern.pattern_at(point(0., 0., 0.));
  Color c2 = pattern.pattern_at(point(.99, 0., 0.));
  Color c3 = pattern.pattern_at(point(1.01, 0., 0.));

  ASSERT_TRUE(c1 == color::white);
  ASSERT_TRUE(c2 == color::white);
  ASSERT_TRUE(c3 == color::black);
}

TEST(patterns_feature, checkers_should_repeat_in_y) {

  CheckersPattern pattern(color::white, color::black);

  Color c1 = pattern.pattern_at(point(0., 0., 0.));
  Color c2 = pattern.pattern_at(point(0., .99, 0.));
  Color c3 = pattern.pattern_at(point(0., 1.01, 0.));

  ASSERT_TRUE(c1 == color::white);
  ASSERT_TRUE(c2 == color::white);
  ASSERT_TRUE(c3 == color::black);
}

TEST(patterns_feature, checkers_should_repeat_in_z) {

  CheckersPattern pattern(color::white, color::black);

  Color c1 = pattern.pattern_at(point(0., 0., 0.));
  Color c2 = pattern.pattern_at(point(0., 0., .99));
  Color c3 = pattern.pattern_at(point(0., 0., 1.01));

  ASSERT_TRUE(c1 == color::white);
  ASSERT_TRUE(c2 == color::white);
  ASSERT_TRUE(c3 == color::black);
}

