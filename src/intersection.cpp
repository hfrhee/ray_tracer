#include "intersection.h"
#include "object.h"

namespace ray_tracer {

  bool Intersection::operator==(const Intersection& other) const {
    bool result;

    if( this->get_object() == nullptr || other.get_object() == nullptr)
      return false;

    result = this->get_object()->get_name() == other.get_object()->get_name();
    double t0 = this->get_t();
    double t1 = other.get_t();
    result = result && ALMOST_EQUAL(t0, t1);

    return result;
  }

  bool Intersection::operator!=(const Intersection& other) const {
    return !(*this == other);
  }
  
  std::optional<const Intersection> hit(const std::vector<Intersection>& xs) {
    const Intersection *result = nullptr;

    if (xs.empty()) return std::nullopt;

    for (const auto& i : xs) {
      if(i.get_t() > 0 && ( result == nullptr || i.get_t() < result->get_t() ) ) {
        result = &i;
      }
    }

    if (result == nullptr) return std::nullopt; 
    return *result;
  }

  Comps Intersection::prepare_computations(const Ray& r) const {
    std::vector<Intersection> xs;
    return zz_prepare_computations(r, xs);
  }

  Comps Intersection::zz_prepare_computations(const Ray& r,
      const std::vector<Intersection>& xs) const {

    Comps result;

    std::vector<std::shared_ptr<Object> > container;

    std::cout << xs.size() << '\n';
    std::cout << container.size() << '\n';

    result.t = this->t;
    result.object = this->object;
  
    result.point = r.position(this->get_t());
    result.eyev = -r.direction;
    result.normalv = this->get_object()->normal_at(result.point);

    if (dot(result.normalv, result.eyev)<0) {
      result.inside = true;
      result.normalv = -result.normalv;
    } else {
      result.inside = false;
    }

    result.reflectv = r.direction.reflect(result.normalv); 

    result.over_point = result.point + result.normalv * EPSILON;

    return result;
  }

}
