#include <iostream>
#include <fstream>


#include "projectile.h"
#include "environment.h"
#include "canvas.h"

using namespace ray_tracer;

Projectile tick(const Environment& env, const Projectile& proj) {
  Projectile result;
  
  result.position = proj.position + proj.velocity;
  result.velocity = proj.velocity + env.gravity + env.wind;

  return result; 
}

int main() {

  Tuple start = point(0., 1., 0.);
  Tuple vel = vector(1., 1.8, 0.).normalize() * 11.25;
  Projectile p(start, vel);

  Environment e(vector(0., -0.1, 0.), vector(-0.01, 0., 0.));
 
  Canvas canvas(900,550);
  
  int n = 0;

  Color red = Color(1., 0., 0.);

  while (p.position[1] >= 0) {
    p = tick(e, p); 
    int x = (int) p.position[0];
    int y = 550 - (int) p.position[1];
    n++;

    if (x < 900 && x >= 0 && y < 550 && y >= 0)
      canvas.write_pixel(x, y, red);
  }

  std::ofstream img("img02.ppm");
  canvas.to_ppm(img);
  img.close();
  return 0;
}
