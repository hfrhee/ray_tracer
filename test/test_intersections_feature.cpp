#include <iostream>
/* #include <typeinfo> */
#include "gtest/gtest.h"
#include "sphere.h"
#include "plane.h"
#include "intersection.h"


using namespace ray_tracer;

TEST(intersections_feature, intersection_encapsulates_t_and_object) {
  /* const auto s = std::make_shared<Sphere>( Sphere{} ); */
  const auto s = Sphere::create();
  Intersection i(3.5, s);

  const auto result = i.get_object()->get_name();
  const auto expected = "Sphere";

  ASSERT_TRUE( i.get_t() == 3.5 );
  ASSERT_STREQ( result.c_str(), expected );
  /* ASSERT_STREQ( typeid(i.get_object()).name(), typeid(s).name()); */
}

TEST(intersections_feature, aggregate_intersections) {
  /* const auto s = std::make_shared<Sphere>( Sphere{} ); */
  const auto s = Sphere::create(); 
  Intersection i1(1., s);
  Intersection i2(2., s);

  std::vector<Intersection> xs {i1, i2};

  /* std::cout << xs.size(); */
  /* std::cout << ", " << xs[0].get_t(); */
  /* std::cout << ", " << xs[1].get_t(); */
  /* std::cout << std::endl; */

  ASSERT_TRUE( xs.size() == 2 );
  ASSERT_TRUE( xs[0].get_t() == 1. );
  ASSERT_TRUE( xs[1].get_t() == 2. );
}

TEST(intersections_feature, intersect_sets_object_on_intersection) {
  /* const auto s = std::make_shared<Sphere>( Sphere{} ); */
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  const auto s = Sphere::create(); 

  std::vector<Intersection> result = s->intersect(r);

  /* std::cout << result.size() << std::endl; */
  /* std::cout << result[0].get_object()->get_name(); */
  /* std::cout << ", " << result[0].get_t(); */
  /* std::cout << std::endl; */

  /* std::cout << result[1].get_object()->get_name(); */
  /* std::cout << ", " << result[1].get_t(); */
  /* std::cout << std::endl; */

  ASSERT_TRUE( result.size() == 2 );
  ASSERT_STREQ( result[0].get_object()->get_name().c_str(), "Sphere" );
  ASSERT_STREQ( result[1].get_object()->get_name().c_str(), "Sphere" );
}

TEST(intersections_feature, hit_all_t_positive) {
  const auto s = Sphere::create(); 

  Intersection i1(1., s);
  Intersection i2(2., s);

  std::vector<Intersection> xs {i1, i2};

  /* Intersection result = hit(xs); */
  auto result = hit(xs);

  ASSERT_TRUE(*result == i1);
}

TEST(intersections_feature, hit_some_t_negative) {
  const auto s = Sphere::create(); 

  Intersection i1(-1., s);
  Intersection i2(1., s);

  std::vector<Intersection> xs {i1, i2};

  /* Intersection result = hit(xs); */
  auto result = hit(xs);

  ASSERT_TRUE(*result == i2);
}

TEST(intersections_feature, hit_all_t_negative) {
  const auto s = Sphere::create(); 

  Intersection i1(-2., s);
  Intersection i2(-1., s);

  std::vector<Intersection> xs {i1, i2};

  /* Intersection result = hit(xs); */
  auto result = hit(xs);

  ASSERT_TRUE(result == std::nullopt);
}

TEST(intersections_feature, hit_resturn_lowest_nonnegative) {
  const auto s = Sphere::create(); 

  Intersection i1(5., s);
  Intersection i2(7., s);
  Intersection i3(-3., s);
  Intersection i4(2., s);

  std::vector<Intersection> xs {i1, i2, i3, i4};

  /* Intersection result = hit(xs); */
  auto result = hit(xs);

  ASSERT_TRUE(result == i4);
}

TEST(intersections_feature, precompute) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  const auto shape = Sphere::create();

  Intersection i(4., shape);

  Comps comps = i.prepare_computations(r);

  ASSERT_TRUE(comps.t == i.get_t());
  ASSERT_TRUE(*comps.object == *(i.get_object()));
  ASSERT_TRUE(comps.point == point(0., 0., -1.));
  ASSERT_TRUE(comps.point == point(0., 0., -1.));
  ASSERT_TRUE(comps.eyev == vector(0., 0., -1.));
  ASSERT_TRUE(comps.normalv == vector(0., 0., -1.));
}

TEST(intersections_feature, hit_when_intersection_outside) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));
  const auto shape = Sphere::create();

  Intersection i(4., shape);

  Comps comps = i.prepare_computations(r);

  ASSERT_TRUE(comps.inside == false);
}

TEST(intersections_feature, hit_when_intersection_inside) {
  Ray r(point(0., 0., 0.), vector(0., 0., 1.));
  const auto shape = Sphere::create();

  Intersection i(1., shape);

  Comps comps = i.prepare_computations(r);

  ASSERT_TRUE(comps.point == point(0., 0., 1.));
  ASSERT_TRUE(comps.eyev == vector(0., 0., -1.));
  ASSERT_TRUE(comps.normalv == vector(0., 0., -1.));
  ASSERT_TRUE(comps.inside == true);
}

TEST(intersections_feature, hit_should_offset_point) {
  Ray r(point(0., 0., -5.), vector(0., 0., 1.));

  const auto shape = Sphere::create();
  shape->set_transform(translation(0., 0., 1.));

  Intersection i(5., shape);

  Comps comps = i.prepare_computations(r);

  ASSERT_TRUE(comps.over_point[2] < EPSILON/2);
  ASSERT_TRUE(comps.over_point[2] < comps.point[2]);
}

TEST(intersections_feature, precomputing_reflection_vector) {
  const auto shape = Plane::create();

  Ray r(point(0., 1., -1.), vector(0, -std::sqrt(2)/2, std::sqrt(2)/2));

  Intersection i(std::sqrt(2), shape);

  Comps comps = i.prepare_computations(r);
  
  ASSERT_TRUE(comps.reflectv == vector(0., std::sqrt(2)/2, std::sqrt(2)/2));
}

TEST(intersections_feature, finding_n1_and_n2_at_various_intersections) {
  const auto A = Sphere::create_glass_sphere();
  A->set_transform(scaling(2., 2., 2.));
  A->get_material().set_refractive_index(1.5);

  const auto B = Sphere::create_glass_sphere();
  B->set_transform(translation(0., 0., -.25));
  B->get_material().set_refractive_index(2.);
  
  const auto C = Sphere::create_glass_sphere();
  C->set_transform(translation(0., 0., .25));
  C->get_material().set_refractive_index(2.5);

  Ray r(point(0., 0., -4.), vector(0., 0., 1.));

  std::vector<Intersection> xs{Intersection(2., A),
                               Intersection(2.75, B),
                               Intersection(3.25, C),
                               Intersection(4.75, B),
                               Intersection(5.25, C),
                               Intersection(6., A),
                              };

  std::array<double, 6> res_n1{1., 1.5, 2., 2.5, 2.5, 1.5};
  std::array<double, 6> res_n2{1.5, 2., 2.5, 2.5, 1.5, 1.};

  for(auto idx=0; idx<6; ++idx) {
    Comps comps = xs[idx].zz_prepare_computations(r, xs);

    EXPECT_TRUE(comps.n1 == res_n1[idx]);
    EXPECT_TRUE(comps.n2 == res_n2[idx]);
  }




  /* Intersection i(std::sqrt(2), shape); */

  /* Comps comps = i.prepare_computations(r); */
  
  /* ASSERT_TRUE(comps.reflectv == vector(0., std::sqrt(2)/2, std::sqrt(2)/2)); */
  ASSERT_TRUE(1 == 1); 
}



